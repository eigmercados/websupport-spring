package br.com.eigmercados.websupport.utils.relatorios;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

import java.util.Iterator;
import java.util.List;

/**
 * Data Souce responsavel por percorrer o header e campos do relatorio para impressao
 *
 * @author rodolfo.martins Criado em: 14/05/2014
 */
public class ColunaDinamicaDataSource extends JRAbstractBeanDataSource {

    private List<RelatorioDinamicoHeaderVO> columnHeaders;
    private List<List<String>> linhas;
    private Iterator<List<String>> iterator;
    private List<String> linhaCorrente;
    private int profundidade = 1;

    public ColunaDinamicaDataSource(RelatorioDinamicoVO relatorioDinamicoVO) {
        super(true);

        this.linhas = relatorioDinamicoVO.getLinhas();
        this.columnHeaders = relatorioDinamicoVO.getHeaders();

        if (this.linhas != null && this.linhas != null) {
            this.iterator = this.linhas.iterator();
        }

        this.profundidade = relatorioDinamicoVO.getProfundidade();
    }

    @Override
    public boolean next() {
        boolean hasNext = false;

        if (iterator != null) {
            hasNext = iterator.hasNext();

            if (hasNext) {
                this.linhaCorrente = iterator.next();
            }
        }

        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField field) throws JRException {
        // The name of the field in dynamic columns that were created by DynamicReportBulder is also the index into the list of columns.
        // For example, if the field is named 'col1', this is the second (because it's zero-based) column in the currentRow.
        String fieldName = field.getName();
        //		return fieldName;
        if (fieldName.startsWith(RelatorioDinamicoBuilder.COL_EXPR_PREFIX)) {
            String[] indexValue = fieldName.substring(RelatorioDinamicoBuilder.COL_EXPR_PREFIX.length()).split("_");
            String column = "-";
            try {
                column = linhaCorrente.get(Integer.parseInt(indexValue[indexValue.length - 1]));
            } catch (IndexOutOfBoundsException e) {
                column = "-";
            }
            return column.isEmpty() ? "-" : column;
        } else if (fieldName.startsWith(RelatorioDinamicoBuilder.COL_HEADER_EXPR_PREFIX)) {
            return this.getFieldValue(fieldName, columnHeaders);
        } else {
            throw new RuntimeException("The field name '" + fieldName + "' in the Jasper Report is not valid");
        }
    }

    @Override
    public void moveFirst() {
        if (linhas != null) {
            iterator = linhas.iterator();
        }
    }

    private String getFieldValue(String fieldName, List<RelatorioDinamicoHeaderVO> lista) {

        String v = null;
        for (RelatorioDinamicoHeaderVO vo : lista) {
            if (vo.getColName().equals(fieldName)) {
                return vo.getLabel();
            } else if (!vo.getSubColunas().isEmpty()) {
                v = getFieldValue(fieldName, vo.getSubColunas());
            }
            if (v != null)
                return v;
        }
        return v;
    }
}
