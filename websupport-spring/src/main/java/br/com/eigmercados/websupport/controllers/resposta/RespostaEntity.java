package br.com.eigmercados.websupport.controllers.resposta;

import br.com.eigmercados.websupport.geral.EigApplicationProvider;
import br.com.eigmercados.websupport.geral.i18n.I18NUtil;
import lombok.Getter;
import org.springframework.context.ApplicationContext;

import java.io.Serializable;
import java.util.Map;

/**
 * criado por bruno em 03/09/17.
 */
@Getter
public class RespostaEntity implements Serializable {

    private static final long serialVersionUID = 4313161695002722794L;

    private String mensagemSucesso;
    private Map<String, String> mensagens;
    private Object resultado;
    private boolean sucesso;

    private RespostaEntity() {
    }

    public static class RespostaEntityBuilder {

        private RespostaEntity respostaEntity = new RespostaEntity();

        public RespostaEntity build() {

            if (respostaEntity.mensagemSucesso != null && !respostaEntity.mensagemSucesso.isEmpty()) {
                respostaEntity.mensagemSucesso = traduzirMensagem(respostaEntity.mensagemSucesso);
            }

            return respostaEntity;
        }

        private String traduzirMensagem(String mensagemSucesso) {

            ApplicationContext applicationContext = EigApplicationProvider.getApplicationContext();
            I18NUtil i18NUtil = applicationContext.getBean(I18NUtil.class);

            return i18NUtil.getProperty(mensagemSucesso, null);
        }

        public RespostaEntityBuilder setMensagens(Map<String, String> mensagens) {
            respostaEntity.mensagens = mensagens;
            return this;
        }

        public RespostaEntityBuilder setObjeto(Object objeto) {
            respostaEntity.resultado = objeto;
            return this;
        }

        public RespostaEntityBuilder setMensagemSucesso(String objeto) {
            respostaEntity.mensagemSucesso = objeto;
            return this;
        }

        public RespostaEntityBuilder setSucesso(boolean sucesso) {
            respostaEntity.sucesso = sucesso;
            return this;
        }
    }

}
