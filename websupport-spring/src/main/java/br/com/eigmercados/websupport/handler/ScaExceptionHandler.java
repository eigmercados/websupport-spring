package br.com.eigmercados.websupport.handler;


import br.com.eigmercados.websupport.controllers.resposta.RespostaEntity;
import br.com.eigmercados.websupport.geral.EigException;
import br.com.eigmercados.websupport.geral.i18n.I18NUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.logging.Logger;


@ControllerAdvice
public class ScaExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private I18NUtil i18NUtil;

    private Logger logger = Logger.getLogger(getClass().getCanonicalName());

    /**
     * Metodo que trata a excecoes do tipo Exception
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<RespostaEntity> handlerSncException(Exception e, WebRequest request) {
        return new ResponseEntity<RespostaEntity>(new RespostaEntity.RespostaEntityBuilder().setMensagemSucesso(getMensagemErro(e)).build(), new HttpHeaders(), e instanceof AccessDeniedException?HttpStatus.UNAUTHORIZED: HttpStatus.BAD_REQUEST);
    }

    /**
     * Pega a msg de erro padrao
     */
    private String getMensagemErro(Exception e) {
        if (e instanceof EigException) {
            return i18NUtil.getProperty(e.getMessage());
        } else {
            logger.severe("-- Excecao nao tratada. Resumo: ");
            logger.severe("- Excecao: " + e.getClass().getCanonicalName());
            logger.severe("- Mensagem: " + e.getMessage());

            if (!"${ambiente.nomePerfilMaven}".contains("Produ")) {
                e.printStackTrace();
            }

            return i18NUtil.getProperty("MSG_E001", ExceptionUtils.getRootCauseMessage(e));
        }
    }
}
