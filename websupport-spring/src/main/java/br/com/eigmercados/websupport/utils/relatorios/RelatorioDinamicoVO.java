/*
 * Projeto: sisagua-core
 * Arquivo: CampoRelatorioDinamicoVO.java
 * 
 * Copyright @ Ministerio da Saude.
 *
 * Este software e confidencial e de propriedade do Ministerio da Saude.
 * Nao e permitida sua distribuicao ou divulgacao do seu conteudo sem 
 * expressa autorizaao do mesmo.
 */
package br.com.eigmercados.websupport.utils.relatorios;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rodolfo.martins Criado em: 14/05/2014
 */
public class RelatorioDinamicoVO {

    private List<RelatorioDinamicoHeaderVO> headers;
    private List<List<String>> linhas;
    private Integer profundidade;

    public RelatorioDinamicoVO() {
        super();
        setHeaders(new ArrayList<RelatorioDinamicoHeaderVO>());
        setLinhas(new ArrayList<List<String>>());
    }

    public RelatorioDinamicoVO(List<RelatorioDinamicoHeaderVO> headers, List<List<String>> linhas) {
        this();
        this.headers = headers;
        this.linhas = linhas;
        this.profundidade = getDeapth(headers);
    }

    public static int getDeapth(List<RelatorioDinamicoHeaderVO> headers) {
        return getDeapth(headers, 1);
    }

    public static int getDeapth(List<RelatorioDinamicoHeaderVO> headers, int depth) {
        for (RelatorioDinamicoHeaderVO header : headers) {
            if (!header.getSubColunas().isEmpty()) {
                depth++;
                return getDeapth(header.getSubColunas(), depth);
            }
        }
        return depth;
    }

    public void addHeader(RelatorioDinamicoHeaderVO relatorioDinamicoHeaderVO) {
        getHeaders().add(relatorioDinamicoHeaderVO);
    }

    public int getProfundidade() {
        if (profundidade == null)
            this.profundidade = getDeapth(this.headers);
        return profundidade;
    }

    public void addLinha(List<String> linha) {
        getLinhas().add(linha);
    }

    public List<RelatorioDinamicoHeaderVO> getHeaders() {
        return headers;
    }

    public void setHeaders(List<RelatorioDinamicoHeaderVO> headers) {
        this.headers = headers;
    }

    public List<List<String>> getLinhas() {
        return linhas;
    }

    public void setLinhas(List<List<String>> linhas) {
        this.linhas = linhas;
    }
}
