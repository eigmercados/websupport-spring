package br.com.eigmercados.websupport.utils.relatorios;

import br.com.eigmercados.websupport.geral.EigApplicationProvider;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Classe Utilitaria para impressao de relatorios
 */
@SuppressWarnings("serial")
public class UtilRelatorio implements Serializable {
    private static final Log logger = LogFactory.getLog(UtilRelatorio.class);


    public static final String FORMAT_HTML = "HTML";
    public static final String FORMAT_PDF = "PDF";
    public static final String FORMAT_XLS = "XLS";
    private static final Object FORMAT_ODS = "ODS";

    private UtilRelatorio() {
        super();
    }

    /**
     * Gera o arquivo JasperPrint para ser usado individualmente ou com a concatencao de outras paginas de relatorio atraves de uma lista.
     *
     * @param nomeArquivo
     * @param listObjetos
     * @param parametrosRelatorio
     * @return
     */
    public static JasperPrint gerarJasperPrint(String nomeArquivo, List<? extends Object> listObjetos, Map<String, Object> parametrosRelatorio) {
        Map<JasperPrint, JRFileVirtualizer> r = gerarJasperPrint2(nomeArquivo, listObjetos, parametrosRelatorio);
        return r.keySet().iterator().next();
    }

    public static Map<JasperPrint, JRFileVirtualizer> gerarJasperPrint2(String nomeArquivo, List<? extends Object> listObjetos, Map<String, Object> parametrosRelatorio) {
        JasperPrint jasperPrint = null;

        ServletContext bean = EigApplicationProvider.getBean(ServletContext.class);

//        String caminhoRelatorio = SnrngApplicationAware.getInstance().getHttpServletRequest().getSession().getServletContext().getRealPath("") + File.separatorChar;
        String caminhoRelatorio = bean.getRealPath("") +"/WEB-INF/classes/"+ File.separatorChar;;

        JRFileVirtualizer virtualizer = null;

        Map<String, Object> parameters = new HashMap<String, Object>();
        try {

            String nomeJasper = nomeArquivo + ".jasper";

            parameters.put("subCaminho", caminhoRelatorio + "relatorios" + File.separatorChar);
            parameters.put("SUBREPORT_DIR", caminhoRelatorio + "relatorios" + File.separatorChar);
            parameters.put("subCaminhoHeaderFooter", caminhoRelatorio + "relatorios" + File.separatorChar);
            parameters.put("imgCaminho", caminhoRelatorio + "img" + File.separatorChar);

            if (parametrosRelatorio != null) {
                parameters.putAll(parametrosRelatorio);
            }

            if (listObjetos == null) {
                jasperPrint = JasperFillManager.fillReport(caminhoRelatorio + "relatorios" + File.separatorChar + nomeJasper, parameters);
            } else {
                jasperPrint = JasperFillManager.fillReport(caminhoRelatorio + "relatorios" + File.separatorChar + nomeJasper, parameters, new JRBeanCollectionDataSource(listObjetos));
            }
            logger.debug("~~~~> QUANTIDADE DE PAGINAS GERADAS: " + jasperPrint.getPages().size());

            Map<JasperPrint, JRFileVirtualizer> r = new HashMap<JasperPrint, JRFileVirtualizer>();
            r.put(jasperPrint, virtualizer);
            return r;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * Gera o JasperPrint de relatorio com colunas dinamicas
     *
     * @param nomeArquivo
     * @param parametrosRelatorio
     * @param relatorioDinamicoVO
     * @return
     */
    public static Map<JasperPrint, JRFileVirtualizer> gerarJasperPrintRelatorioDinamico(String nomeArquivo, Map<String, Object> parametrosRelatorio, RelatorioDinamicoVO relatorioDinamicoVO) {

        InputStream input;
        JasperDesign jasperReportDesign;
        RelatorioDinamicoBuilder reportBuilder;
        JasperReport jasperReport;
        ColunaDinamicaDataSource pdfDataSource;
        JasperPrint jasperPrint;

        ServletContext bean = EigApplicationProvider.getBean(ServletContext.class);
        String caminhoRelatorio = bean.getRealPath("") +"/WEB-INF/classes/"+ File.separatorChar;

        JRFileVirtualizer virtualizer;

        try {

            virtualizer = new JRFileVirtualizer(5, caminhoRelatorio); //new JRSwapFileVirtualizer(2, swapFile, true);
            JRVirtualizationHelper.setThreadVirtualizer(virtualizer);

            String nomeJrxml = nomeArquivo + ".jrxml";
            input =bean.getResourceAsStream("/relatorios/" + nomeJrxml);
            jasperReportDesign = JRXmlLoader.load(input);

            reportBuilder = new RelatorioDinamicoBuilder(jasperReportDesign, relatorioDinamicoVO.getHeaders());
            reportBuilder.addColunasDinamicas();
            jasperReport = JasperCompileManager.compileReport(jasperReportDesign);

            if (parametrosRelatorio != null && !parametrosRelatorio.isEmpty()) {
                parametrosRelatorio = new HashMap<String, Object>();
            }
            parametrosRelatorio.put("subCaminho", caminhoRelatorio + "relatorios" + File.separatorChar);
            parametrosRelatorio.put("SUBREPORT_DIR", caminhoRelatorio + "relatorios" + File.separatorChar);
            parametrosRelatorio.put("subCaminhoHeaderFooter", caminhoRelatorio + "relatorios" + File.separatorChar);
            parametrosRelatorio.put("imgCaminho", caminhoRelatorio + "img" + File.separatorChar);

            parametrosRelatorio.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
            parametrosRelatorio.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);

            pdfDataSource = new ColunaDinamicaDataSource(relatorioDinamicoVO);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosRelatorio, pdfDataSource);
            Map<JasperPrint, JRFileVirtualizer> r = new HashMap<JasperPrint, JRFileVirtualizer>();
            r.put(jasperPrint, virtualizer);
            return r;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    public static File exportarRelatorioStreamedContent(String nomeArquivo, List<JasperPrint> jasperPrintList, String tipoRelatorio) {
        return exportarRelatorioStreamedContent(nomeArquivo, jasperPrintList, tipoRelatorio, null);
    }

    /**
     * Exporta um relatorio de um ou n paginas
     *
     * @param nomeArquivo
     * @param jasperPrintList
     * @param tipoRelatorio
     * @param virtualizer
     * @return
     */
    public static File exportarRelatorioStreamedContent(String nomeArquivo, List<JasperPrint> jasperPrintList, String tipoRelatorio, JRFileVirtualizer virtualizer) {
        OutputStream arquivoRetorno = null;
        Locale locale = new Locale("pt", "BR");
        Locale.setDefault(locale);
        Exporter exporter = null;
        String extensaoArquivoExportado = "";
        ServletContext bean = EigApplicationProvider.getBean(ServletContext.class);
        String caminhoRelatorio = bean.getRealPath("") +"/WEB-INF/classes/"+ File.separatorChar;

        File arquivoGerado;

        try {

            if (tipoRelatorio.equals(FORMAT_PDF)) {
                exporter = new JRPdfExporter();
                extensaoArquivoExportado = "pdf";
            } else if (tipoRelatorio.equals(FORMAT_XLS)) {
                exporter = new JRXlsxExporter();
                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setSheetNames(new String[]{"relatorioo"});
                configuration.setIgnorePageMargins(true);
                configuration.setCollapseRowSpan(true);
                configuration.setRemoveEmptySpaceBetweenColumns(true);
                configuration.setRemoveEmptySpaceBetweenRows(true);
                configuration.setWhitePageBackground(false);
                configuration.setFontSizeFixEnabled(true);
                configuration.setOnePagePerSheet(false);

                exporter.setConfiguration(configuration);

                extensaoArquivoExportado = "xlsx";
            } else if (tipoRelatorio.equals(FORMAT_HTML)) {
                exporter = new HtmlExporter();
                SimpleHtmlReportConfiguration configuration = new SimpleHtmlReportConfiguration();
                configuration.setRemoveEmptySpaceBetweenRows(false);
                extensaoArquivoExportado = "html";
            } else if (tipoRelatorio.equals(FORMAT_ODS)) {
                exporter = new JROdtExporter();
                extensaoArquivoExportado = "ods";
            }

//            arquivoGerado = new File(caminhoRelatorio + File.separator + nomeArquivo + "." + extensaoArquivoExportado);
            arquivoGerado = File.createTempFile(nomeArquivo,"."+extensaoArquivoExportado);

            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(arquivoGerado);
            exporter.setExporterOutput(output);
            exporter.exportReport();

            return arquivoGerado;

        } catch (Exception e) {
            logger.error(e);
            return null;
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
        }
    }

    /**
     * Gera o relatorio de uma unica pagina baseado nos parametros informados
     *
     * @param nomeArquivo
     * @param listObjetos
     * @param parametrosRelatorio
     * @param tipoRelatorio
     * @return
     */
    public static File gerarRelatorioStreamedContent(String nomeArquivo, List<? extends Object> listObjetos, Map<String, Object> parametrosRelatorio, String tipoRelatorio) {
        JasperPrint jasperPrint = null;
        JRFileVirtualizer virtualizer = null;
        try {
            Map<JasperPrint, JRFileVirtualizer> r = gerarJasperPrint2(nomeArquivo, listObjetos, parametrosRelatorio);
            jasperPrint = r.keySet().iterator().next();
            virtualizer = r.get(jasperPrint);
            return exportarRelatorioStreamedContent(nomeArquivo, Arrays.asList(jasperPrint), tipoRelatorio, virtualizer);
        } catch (Exception e) {
            logger.error(e);
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
        }
        return null;
    }

    /**
     * Gera o relatorio com colunas dinamicas
     *
     * @param nomeArquivo
     * @param parametrosRelatorio
     * @param relatorioDinamicoVO
     * @param tipoRelatorio
     * @return
     */
    public static File gerarRelatorioDinamicoStreamedContent(String nomeArquivo, Map<String, Object> parametrosRelatorio, RelatorioDinamicoVO relatorioDinamicoVO, String tipoRelatorio) {
        Map<JasperPrint, JRFileVirtualizer> r = gerarJasperPrintRelatorioDinamico(nomeArquivo, parametrosRelatorio, relatorioDinamicoVO);
        JasperPrint jasperPrint = r.keySet().iterator().next();

        return exportarRelatorioStreamedContent(nomeArquivo, Arrays.asList(jasperPrint), tipoRelatorio, r.get(jasperPrint));
    }

}