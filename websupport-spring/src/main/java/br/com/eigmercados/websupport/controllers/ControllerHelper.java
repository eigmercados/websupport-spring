package br.com.eigmercados.websupport.controllers;


import br.com.eigmercados.websupport.controllers.resposta.RespostaEntity;
import br.com.eigmercados.websupport.controllers.resposta.RespostaEntity.RespostaEntityBuilder;
import br.com.eigmercados.websupport.geral.i18n.I18NUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * criado por bruno em 03/09/17.
 */
@Component
public abstract class ControllerHelper {
    protected final Log logger = LogFactory.getLog(getClass());

    protected static final String ERRO="ERRO";
    protected static final String INSERIDO="MSG_002";
    protected static final String ALTERADO="MSG_003";
    protected static final String EXCLUIDO="MSG_004";

    @Autowired
    private I18NUtil i18NUtil;


    protected final String JSON=MediaType.APPLICATION_JSON_VALUE;

    //TODO: FAZER UMA FACTORY PARA O RESPOSTA
    protected ResponseEntity<RespostaEntity> criaMensagemResposta(String msgSucesso,Map<String,String> mensagens,Object objeto) {
        return criaMensagemResposta(msgSucesso, mensagens, objeto,true);
    }

    protected ResponseEntity<RespostaEntity> criaMensagemResposta(String msg,Map<String,String> mensagens,Object objeto,Boolean sucesso) {
        return ResponseEntity.ok(new RespostaEntityBuilder().setMensagens(mensagens).setObjeto(objeto).setMensagemSucesso(msg).setSucesso(sucesso).build());
    }

    /**
     * Cria a mensagem com os erros especificados
     * Seta o sucesso como false
     * @param mensagens
     * @param objeto
     * @return
     */
    protected ResponseEntity<RespostaEntity> criaMensagemResposta(Map<String,String> mensagens,Object objeto){
        if(!mensagens.isEmpty()){
            return criaMensagemResposta(mensagens.get(ERRO),mensagens,objeto,false);
        }
        return ResponseEntity.ok(new RespostaEntityBuilder().setMensagens(mensagens).setObjeto(objeto).setSucesso(true).build());
    }

    protected String getMensagem(String key, String... params) {
        return i18NUtil.getProperty(key, params);
    }

}
