package br.com.eigmercados.websupport.configuracao.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

@Configuration
//@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "rest";

	@Autowired
	private TokenStore tokenStore;

	@Bean
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore);
		return defaultTokenServices;
	}

	public OAuth2AuthenticationManager auth2AuthenticationManager() {
		OAuth2AuthenticationManager oAuth2AuthenticationManager = new OAuth2AuthenticationManager();
		oAuth2AuthenticationManager.setTokenServices(tokenServices());
		return oAuth2AuthenticationManager;
	}

	@Bean
	public OAuth2AuthenticationProcessingFilter auth2AuthenticationProcessingFilter() throws Exception {
		OAuth2AuthenticationProcessingFilter oAuth2AuthenticationProcessingFilter = new OAuth2AuthenticationProcessingFilterSNC();
		oAuth2AuthenticationProcessingFilter.setAuthenticationManager(auth2AuthenticationManager());
		return oAuth2AuthenticationProcessingFilter;
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
		resources.authenticationManager(auth2AuthenticationManager());
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
        /*http.addFilterAfter(auth2AuthenticationProcessingFilter(), SecurityContextPersistenceFilter.class)
                        .anonymous().disable()
                        .requestMatchers().antMatchers("/rest/**")
                        .and()
                        .authorizeRequests()
                        .antMatchers("/rest/**").authenticated()
                        .antMatchers("/rest/empresa/lista","/rest/sistema/lista").permitAll();*/

		http.addFilterAfter(auth2AuthenticationProcessingFilter(), SecurityContextPersistenceFilter.class)
						.authorizeRequests()
						.antMatchers("/rest/empresa/lista", "/rest/sistema/lista",
										"/rest/usuario/pessoa/**","/rest/perfil/sistema/**",
										"/rest/usuario","/rest/apoio/logradouro/cep/**")
						.permitAll()
						.and()
						.authorizeRequests().anyRequest().authenticated();

	}

}