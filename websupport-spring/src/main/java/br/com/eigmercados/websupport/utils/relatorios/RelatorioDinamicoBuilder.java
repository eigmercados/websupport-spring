package br.com.eigmercados.websupport.utils.relatorios;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRLineBox;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.type.*;

import java.awt.*;
import java.util.List;

/**
 * Responsavel por adicionar os campos em tempo de execucao dos relatorios dinamicos
 *
 * @author rodolfo.martins Criado em: 14/05/2014
 */
public class RelatorioDinamicoBuilder {

    // The prefix used in defining the field name that is later used by the JasperFillManager
    public static final String COL_EXPR_PREFIX = "col";

    // The prefix used in defining the column header name that is later used by the JasperFillManager
    public static final String COL_HEADER_EXPR_PREFIX = "header";

    // The whitespace between columns in pixels
    private final static int SPACE_BETWEEN_COLS = 0;

    // The height in pixels of an element in a row and column
    private final static int COLUMN_HEIGHT = 25;

    // The total height of the column header or detail band
    private final static int BAND_HEIGHT = 25;

    private final static int HEADER_HEIGHT = 28;

    // The left and right margin in pixels
    private final static int MARGIN = 0;

    // The JasperDesign object is the internal representation of a report
    private JasperDesign jasperDesign;

    // definicoes das colunas do relatorio
    private List<RelatorioDinamicoHeaderVO> headers;

    private int depth = 1;
    private int bandCount = 0;
    private JRDesignBand detailBand = new JRDesignBand();
    private JRDesignBand headerBand = new JRDesignBand();

    private JRDesignStyle normalStyle = getNormalStyle();
    private JRDesignStyle columnHeaderStyle = getColumnHeaderStyle();

    public RelatorioDinamicoBuilder(JasperDesign jasperDesign, List<RelatorioDinamicoHeaderVO> headers) {
        this.jasperDesign = jasperDesign;
        this.headers = headers;
    }

    @SuppressWarnings("deprecation")
    public void addColunasDinamicas() throws JRException {

        depth = RelatorioDinamicoVO.getDeapth(headers);

        headerBand.setHeight(HEADER_HEIGHT * (depth + 1));
        detailBand.setHeight(BAND_HEIGHT);

        jasperDesign.addStyle(normalStyle);
        jasperDesign.addStyle(columnHeaderStyle);

        int xPos = MARGIN;
        for (int i = 0; i < headers.size(); i++) {
            RelatorioDinamicoHeaderVO headerVO = headers.get(i);

            int columnWidth = getWidth(headerVO);

            if (headerVO.getSubColunas().isEmpty()) {
                int dt = 1;
                do {
                    JRDesignTextField head = addHeaderColl(headerVO, dt + "_" + i, columnWidth, xPos, HEADER_HEIGHT * dt, dt == depth && depth > 1);
                    if (headerVO.getColName() == null || headerVO.getColName().isEmpty())
                        headerVO.setColName(COL_HEADER_EXPR_PREFIX + (dt + "_" + i));
                    dt++;
                } while (dt <= depth);

            } else {
                addDeepColumn(headerVO, "1_" + i + "", xPos, HEADER_HEIGHT);
            }

            //addBandCol(index+"", columnWidth, xPos, normalStyle, detailBand);

            xPos = xPos + columnWidth + SPACE_BETWEEN_COLS;
        }

        jasperDesign.setColumnHeader(headerBand);
        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(detailBand);
    }

    private JRDesignTextField addHeaderColl(RelatorioDinamicoHeaderVO headerVO, String i, int columnWidth, int xPos, int yPos, boolean blank)
                    throws JRException {
        return addHeaderColl(headerVO, i, columnWidth, xPos, yPos, HEADER_HEIGHT, blank);
    }

    private JRDesignTextField addHeaderColl(RelatorioDinamicoHeaderVO headerVO, String i, int columnWidth, int xPos, int yPos, int heigth, boolean blank)
                    throws JRException {

        // Create a Header Field
        JRDesignField headerField = new JRDesignField();
        headerField.setName(COL_HEADER_EXPR_PREFIX + i);
        headerField.setValueClass(java.lang.String.class);
        jasperDesign.addField(headerField);

        JRDesignTextField colHeaderField = new JRDesignTextField();
        colHeaderField.setX(xPos);
        colHeaderField.setY(yPos);
        colHeaderField.setWidth(columnWidth);
        colHeaderField.setHeight(heigth);
        colHeaderField.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
        //colHeaderField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        colHeaderField.setStyle(columnHeaderStyle);
        addBorder(colHeaderField);

        colHeaderField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        colHeaderField.setPositionType(PositionTypeEnum.FLOAT);
        colHeaderField.setEvaluationTime(EvaluationTimeEnum.NOW);
        colHeaderField.setStretchWithOverflow(true);
        colHeaderField.setPrintWhenDetailOverflows(true);
        colHeaderField.setStretchType(StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT);

        JRDesignExpression headerExpression = new JRDesignExpression();
        headerExpression.setValueClass(java.lang.String.class);
        headerExpression.setText(!blank ? "$F{" + COL_HEADER_EXPR_PREFIX + i + "}" : "\"\"");
        colHeaderField.setExpression(headerExpression);
        //show the column header only at the first page
        colHeaderField.setPrintWhenExpression(new JRDesignExpression("$V{PAGE_NUMBER} == 1"));

        headerBand.addElement(colHeaderField);
        if (!blank && headerVO.getSubColunas().isEmpty())
            addBandCol(i + "", columnWidth, xPos, normalStyle, detailBand);
        return colHeaderField;
    }

    private void addBandCol(String i, int columnWidth, int xPos, JRDesignStyle normalStyle, JRDesignBand detailBand)
                    throws JRException {

        // Create a Column Field
        JRDesignField field = new JRDesignField();
        field.setName(COL_EXPR_PREFIX + bandCount);
        field.setValueClass(java.lang.String.class);
        jasperDesign.addField(field);

        // Add text field to the detailBand
        JRDesignTextField textField = new JRDesignTextField();
        textField.setX(xPos);
        textField.setY(0);
        textField.setWidth(columnWidth);
        textField.setHeight(COLUMN_HEIGHT);
        textField.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
        textField.setStyle(normalStyle);
        textField.setBlankWhenNull(true);

        addBorder(textField);

        textField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        textField.setPositionType(PositionTypeEnum.FLOAT);
        textField.setEvaluationTime(EvaluationTimeEnum.NOW);
        textField.setStretchWithOverflow(true);
        textField.setPrintWhenDetailOverflows(true);
        textField.setStretchType(StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText("$F{" + COL_EXPR_PREFIX + bandCount + "}");
        textField.setExpression(expression);
        detailBand.addElement(textField);
        bandCount++;
    }

    private JRDesignStyle getNormalStyle() {
        JRDesignStyle normalStyle = new JRDesignStyle();
        normalStyle.setName("Sans_Normal");
        normalStyle.setDefault(true);
        normalStyle.setFontName("SansSerif");
        normalStyle.setFontSize(7);
        normalStyle.setPdfFontName("Helvetica");
        normalStyle.setPdfEncoding("Cp1252");
        normalStyle.setPdfEmbedded(false);
        return normalStyle;
    }

    private JRDesignStyle getColumnHeaderStyle() {
        JRDesignStyle columnHeaderStyle = new JRDesignStyle();
        columnHeaderStyle.setName("Sans_Header");
        columnHeaderStyle.setDefault(false);
        columnHeaderStyle.setFontName("SansSerif");
        columnHeaderStyle.setFontSize(8);
        columnHeaderStyle.setBold(true);
        columnHeaderStyle.setPdfFontName("Helvetica");
        columnHeaderStyle.setPdfEncoding("Cp1252");
        columnHeaderStyle.setPdfEmbedded(false);

        columnHeaderStyle.setBackcolor(new Color(204, 204, 204));// cinza
        columnHeaderStyle.setMode(ModeEnum.OPAQUE);

        return columnHeaderStyle;
    }

    private void addBorder(JRDesignTextField field) {
        float bw = .5f;
        Color c = Color.BLACK;
        JRLineBox box = field.getLineBox();
        box.getLeftPen().setLineWidth(bw);
        box.getLeftPen().setLineColor(c);
        box.getRightPen().setLineWidth(bw);
        box.getRightPen().setLineColor(c);
        box.getBottomPen().setLineWidth(bw);
        box.getBottomPen().setLineColor(c);
        box.getTopPen().setLineWidth(bw);
        box.getTopPen().setLineColor(c);
    }

    public int getWidth(RelatorioDinamicoHeaderVO coluna) {
        if (coluna.getSubColunas().isEmpty())
            return coluna.getTamanho();
        return getWidth(coluna, 0);
    }

    private int getWidth(RelatorioDinamicoHeaderVO coluna, int ini) {
        if (coluna.getSubColunas().isEmpty())
            return ini + coluna.getTamanho();

        for (RelatorioDinamicoHeaderVO vo : coluna.getSubColunas()) {
            ini = getWidth(vo, ini);
        }
        return ini;
    }

    private void addDeepColumn(RelatorioDinamicoHeaderVO coluna, String index, int xPos, int yPos) {
        int ii = 1;
        int xt = xPos;

        try {
            //adiciona a coluna mais alta
            coluna.setColName(COL_HEADER_EXPR_PREFIX + index + "_" + ii);
            addHeaderColl(coluna, index + "_" + ii, getWidth(coluna), xt, yPos, false);
            //anda a posicao y em um andar
            int ypos = yPos + HEADER_HEIGHT;
            int a = 1;
            for (RelatorioDinamicoHeaderVO col : coluna.getSubColunas()) {
                ii++;
                int tamanho = getWidth(col);
                if (!col.getSubColunas().isEmpty()) {
                    addDeepColumn(col, index + "_" + ii, xt, ypos);
                } else {
                    col.setColName(COL_HEADER_EXPR_PREFIX + index + "_" + ii + "_" + a);
                    addHeaderColl(col, index + "_" + ii + "_" + a, tamanho, xt, ypos, false);
                }
                xt += tamanho + SPACE_BETWEEN_COLS;
                a++;
            }

        } catch (JRException e) {
            e.printStackTrace();
        }

    }

    public int getDepth() {
        return depth;
    }
}
