package br.com.eigmercados.websupport.configuracao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleContextResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;

@Configuration
public class WebConfiguracao extends WebMvcConfigurerAdapter {

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new Formatter<LocalDate>() {

			@Override
			public LocalDate parse(String text, Locale locale) {
				text = text.replaceAll("\"", "");
				LocalDate retorno = null;
				try {
					retorno = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(text));
				} catch (DateTimeParseException e) {
					try {
						LocalDateTime datetime = LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(text));
						retorno = datetime.toLocalDate();
					} catch (Exception ee) {
						retorno = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(text.substring(0, 10)));
					}
				}

				return retorno;
			}

			@Override
			public String print(LocalDate object, Locale locale) {
				return DateTimeFormatter.ISO_LOCAL_DATE.format(object);
			}
		});
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
						.allowedOrigins("*")
						.allowedHeaders("*")
						.allowedMethods("*")
						.allowCredentials(true);
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		for (HttpMessageConverter converter : converters) {
			if (converter instanceof org.springframework.http.converter.json.MappingJackson2HttpMessageConverter) {
				ObjectMapper mapper = ((MappingJackson2HttpMessageConverter) converter).getObjectMapper();
				//Registering Hibernate4Module to support lazy objects
				Hibernate5Module module = new Hibernate5Module();

                /*SimpleModule dataModule = new SimpleModule("LocalDateTimeSerialize", new Version(1, 0, 0, null));
                dataModule.addSerializer(LocalDate.class, new LocalDateSerializer());
                dataModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());

                dataModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
                dataModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());

                dataModule.addSerializer(LocalTime.class, new LocalTimeSerializer());
                dataModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer());*/

				module.enable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);

				mapper.registerModules(module, new Jdk8Module());

				//        messageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
				mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

				mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);

				mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				mapper.setDateFormat(dateFormat);
			}
		}
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(100 * 1000000);
		try {
			resolver.setUploadTempDir(new FileSystemResource(System.getProperty("java.io.tmpdir")));
		} catch (IOException e) {

		}
		resolver.setDefaultEncoding("utf-8");
		return resolver;
	}

	@Bean(name = "localeResolver")
	public LocaleContextResolver getLocaleContextResolver() {

		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(new Locale("pt", "BR"));
		return sessionLocaleResolver;
	}
}