package br.com.eigmercados.websupport.configuracao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication(scanBasePackages = "br.com.eigmercados")
@EnableJpaRepositories(basePackages = "br.com.eigmercados.*.repositorios")
@EntityScan(basePackages = { "br.com.eigmercados.*.model", "br.com.eigmercados.*.converters" })
@ComponentScan(basePackages = "br.com.eigmercados")
@ImportResource(value = { "classpath:spring-beans.xml" })
@EnableTransactionManagement
@EnableWebMvc
@EnableAsync
@EnableAspectJAutoProxy
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
@EnableWebSecurity
@EnableResourceServer
public class AppInitializer {

	public static void main(String[] args) {
		SpringApplication.run(new Class[] { AppInitializer.class }, args);
	}

}
