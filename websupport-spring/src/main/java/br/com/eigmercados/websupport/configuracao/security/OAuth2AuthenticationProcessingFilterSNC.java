package br.com.eigmercados.websupport.configuracao.security;

import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * criado por bruno em 25/11/17.
 */
public class OAuth2AuthenticationProcessingFilterSNC extends OAuth2AuthenticationProcessingFilter {


    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
                    throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse)res;

        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept, X-Requested-With");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Allow", "POST,GET,OPTIONS,PUT");
        response.setHeader("Access-Control-Max-Age", "3600");

        chain.doFilter(req,res);

    }


}
