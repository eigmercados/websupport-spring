/*
 * Projeto: sisagua-core
 * Arquivo: RelatorioDinamicoHeaderVO.java
 * 
 * Copyright @ Ministerio da Saude.
 *
 * Este software e confidencial e de propriedade do Ministerio da Saude.
 * Nao e permitida sua distribuicao ou divulgacao do seu conteudo sem 
 * expressa autorizaao do mesmo.
 */
package br.com.eigmercados.websupport.utils.relatorios;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rodolfo.martins Criado em: 15/05/2014
 */
public class RelatorioDinamicoHeaderVO implements Comparable<RelatorioDinamicoHeaderVO> {

    private int ordem;
    private String label;
    private int tamanho;
    private String colName;
    private List<RelatorioDinamicoHeaderVO> subColunas = new ArrayList<RelatorioDinamicoHeaderVO>();

    public RelatorioDinamicoHeaderVO(int ordem, String label, int tamanho) {
        super();
        this.ordem = ordem;
        this.label = label;
        this.tamanho = tamanho;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public void addColuna(RelatorioDinamicoHeaderVO coluna) {
        subColunas.add(coluna);
    }

    public List<RelatorioDinamicoHeaderVO> getSubColunas() {
        return subColunas;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    @Override
    public int compareTo(RelatorioDinamicoHeaderVO o) {
        if (this.ordem < o.ordem) {
            return -1;
        } else if (this.ordem > o.ordem) {
            return 1;
        }

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelatorioDinamicoHeaderVO that = (RelatorioDinamicoHeaderVO) o;

        if (ordem != that.ordem) return false;
        if (tamanho != that.tamanho) return false;
        if (label != null ? !label.equals(that.label) : that.label != null) return false;
        return colName != null ? colName.equals(that.colName) : that.colName == null;
    }

    @Override
    public int hashCode() {
        int result = ordem;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + tamanho;
        result = 31 * result + (colName != null ? colName.hashCode() : 0);
        return result;
    }
}
