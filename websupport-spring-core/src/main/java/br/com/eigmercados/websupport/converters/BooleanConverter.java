package br.com.eigmercados.websupport.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class BooleanConverter implements AttributeConverter<Boolean, Boolean> {

	@Override
	public Boolean convertToDatabaseColumn(Boolean aBoolean) {
		return aBoolean == null ? Boolean.FALSE : aBoolean;
	}

	@Override
	public Boolean convertToEntityAttribute(Boolean o) {
		return o == null ? false : o;
	}
}
