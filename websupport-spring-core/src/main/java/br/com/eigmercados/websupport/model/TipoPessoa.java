package br.com.eigmercados.websupport.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Map;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoPessoa {

    FISICA("F", "Física"), JURIDICA("J", "Judírica");

    private String codigo;
    private String descricao;

    private TipoPessoa(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static TipoPessoa getTipoPessoa(String codigo) {
        for (TipoPessoa e : values()) {
            if (e.codigo.equals(codigo)) {
                return e;
            }
        }
        return null;
    }

    @JsonCreator
    public static TipoPessoa fromString(Map nome) {
        if(nome!=null){
            return TipoPessoa.getTipoPessoa((String) nome.get("codigo"));
        }
        return null;
    }
}
