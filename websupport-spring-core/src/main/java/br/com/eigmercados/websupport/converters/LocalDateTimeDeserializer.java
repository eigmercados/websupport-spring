package br.com.eigmercados.websupport.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Deserializador que deve ser usado em todas as entidades que seram recebidas
 * atraves de uma requisição REST nas controllers da aplicação.
 * Ele trata o formato padrao de data  recebidos na requisição
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {


    @Override
    public LocalDateTime deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {

        LocalDateTime retorno = null;
        try {
            retorno = LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(jsonparser.getText()));
        } catch (Exception e) {

        }

        return retorno;
    }
}