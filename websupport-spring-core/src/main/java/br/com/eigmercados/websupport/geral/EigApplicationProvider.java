package br.com.eigmercados.websupport.geral;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class EigApplicationProvider implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    @SuppressWarnings("static-access")
    public void setApplicationContext(ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }

    /**
     * Metodo que retorna o bean gerenciado pelo Spring
     *
     * @param Classe do bean gerenciado
     * @return Bean gerenciado
     */
    public static <T> T getBean(Class<T> bean) {
        if (getApplicationContext() != null) {
            try {
                return applicationContext.getBean(bean);
            } catch (BeansException e) {
                return null;
            }
        }
        return null;
    }
}
