package br.com.eigmercados.websupport.repositorios;

import br.com.eigmercados.websupport.geral.EigApplicationProvider;
import org.hibernate.Session;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.persistence.EntityManager;


/**
 * Interface que define um repositrio de entidades.
 *
 * @param <E> Tipo da entidade.
 * @author bruno.canto
 */
@NoRepositoryBean
public interface Repositorio<E> extends PagingAndSortingRepository<E, Long> {

    default Session getSession() {
        EntityManager entityManager = EigApplicationProvider.getApplicationContext().getBean(EntityManager.class);
        return (Session) entityManager.getDelegate();
    }

}
