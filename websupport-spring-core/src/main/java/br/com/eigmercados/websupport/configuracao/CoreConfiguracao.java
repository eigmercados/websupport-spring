package br.com.eigmercados.websupport.configuracao;

import br.com.eigmercados.websupport.configuracao.security.UsuarioLogado;
import br.com.eigmercados.websupport.geral.EigApplicationProvider;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.HashSet;

@Configuration("configuracaoPrincipal")
public class CoreConfiguracao {

    @Bean
    public EigApplicationProvider applicationContextProvider() {
        return new EigApplicationProvider();
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("msg-negocio");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    @Bean("usuarioLogado")
    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UsuarioLogado criarUsuarioLoado() {
        try {
            return (UsuarioLogado) SecurityContextHolder.getContext()
                            .getAuthentication().getPrincipal();
        } catch (Exception e) {
            return (UsuarioLogado) new User("", "", new HashSet<>());
            //            return new UsuarioSistema(null, "foo", "foo", new HashSet<>());
        }
    }

}