package br.com.eigmercados.websupport.repositorios;

import br.com.eigmercados.websupport.model.EntidadeBasica;
import br.com.eigmercados.websupport.utils.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tools.devnull.trugger.reflection.Reflection;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Implementao generica de um repositrio.
 *
 * @param <E> Tipo da entidade.
 * @author bruno.canto
 */
@Repository
@Transactional(readOnly = true)
public class RepositorioGenerico<E extends EntidadeBasica> {

	private final static int limiteClausulaIn = 900;

	/**
	 * Classe da entidade.
	 */
	private Class<E> entityClass;
	private Class<?> entityClassAlternativo;

	/**
	 * EntityManager usado para a persistencia.
	 */
	@PersistenceContext
	private EntityManager entityManager;

	public RepositorioGenerico() {
		this.entityClass = Reflection.reflect().genericType("E").in(this);
	}

	/**
	 * @return objeto Session do Hibernate referente ao {@link #entityManager}.
	 */

	protected Session getSession() {
		return (Session) entityManager.getDelegate();
	}

	private Class<?> getEntityClass() {
		if (entityClassAlternativo != null) {
			return entityClassAlternativo;
		}
		return entityClass;
	}

	/**
	 * Obtem a entidade pelo seu primaryKey
	 *
	 * @param id id para a busca.
	 * @return
	 */
	public E obterPorId(Serializable id) {
		return (E) entityManager.find(getEntityClass(), id);
	}

	protected <F extends EntidadeBasica> F obterPorId(Class<F> clazz, Serializable id) {
		entityClassAlternativo = clazz;
		try {
			return (F) obterPorId(id);
		} catch (Exception e) {
			EigLogger.error(e);
		} finally {
			entityClassAlternativo = null;
		}
		return null;
	}

	protected <F extends EntidadeBasica> F obterPorId(Class<F> clazz, Serializable id, FetchParametro... fetchs) {
		entityClassAlternativo = clazz;
		try {
			return (F) obterPorId(id, fetchs);
		} catch (Exception e) {
			EigLogger.error(e);
		} finally {
			entityClassAlternativo = null;
		}
		return null;
	}

	/**
	 * Obtem a entidade pelo seu primaryKey e carrega os relacionamentos
	 *
	 * @param id
	 * @param fetchs
	 * @return
	 */
	@Transactional
	public E obterPorId(Serializable id, FetchParametro... fetchs) {
		try {
			String chave = getKey(getEntityClass());

			if (chave != null) {
				return recuperarPorAtributo(chave, id, fetchs);
			}
			return obterPorId(id);
		} catch (Exception e) {
			EigLogger.error(e);
			return null;
		}
	}

	private String getKey(Class<?> clazz) {
		Field[] declaredFields = clazz.getDeclaredFields();
		String chave = null;
		for (Field declaredField : declaredFields) {
			if (declaredField.isAnnotationPresent(Id.class) || declaredField.isAnnotationPresent(EmbeddedId.class)) {
				chave = declaredField.getName();
				break;
			}
		}
		return chave;
	}

	/**
	 * listar todos da entidade
	 *
	 * @return
	 */
	public List<E> listar() {
		try {
			return getSession().createCriteria(getEntityClass()).list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	public List<E> listar(FetchParametro... fetchs) {
		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			setFetchs(criteria, fetchs);
			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	public Page<E> listar(int pagina, int max) {
		return listar(PageRequest.of(pagina, max));
	}

	public Page<E> listar(PageRequest paginacao) {
		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			criteria.setProjection(Projections.rowCount());

			Long total = (Long) criteria.uniqueResult();
			Page<E> page;
			criteria.setProjection(null);
			if (total > 0) {
				page = new PageImpl<E>(criteria.list(), paginacao, total);
			} else {
				page = new PageImpl<E>(Collections.emptyList(), paginacao, total);
			}

			return page;
		} catch (HibernateException e) {
			EigLogger.error(e);
			return null;
		}
	}

	public List<E> pesquisar(E entidade) {

		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			criarExamples(criteria, entidade, "");
			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	private <T extends EntidadeBasica> void criarExamples(Criteria criteria, T entidade, String path) {
		if (entidade != null) {
			if (path.isEmpty()) {
				criteria.add(Example.create(entidade).ignoreCase().setPropertySelector(selectorNaoBooleansNulos));
			}

			Field[] campos = entidade.getClass().getDeclaredFields();

			for (Field campo : campos) {
				if (Modifier.isStatic(campo.getModifiers()) || Modifier.isTransient(campo.getModifiers())
								|| Modifier.isFinal(campo.getModifiers()) || campo.isAnnotationPresent(Transient.class)) {
					continue;
				}
				campo.setAccessible(true);
				try {
					Object obj = campo.get(entidade);
					if (obj != null) {

						if (obj instanceof Collection) {
							String finalPath = campo.getName();
							//							String pth = (finalPath.isEmpty() ? campo.getName() : (".") + campo.getName());
							Criteria crit = criteria.createCriteria(finalPath, JoinType.LEFT_OUTER_JOIN);
							((Iterable) obj).forEach(o -> {
								criarExamples(crit, (T) o, finalPath);
							});
						}

						String key = getKey(entidade.getClass());
						if (obj instanceof EntidadeBasica) {
							if (EntidadeUtils.isEntidadeVazia((EntidadeBasica) obj, false)) {
								continue;
							}
							/*if (!path.isEmpty()) {
								path += (path.isEmpty() ? campo.getName() : (".") + campo.getName());
							}*/
							path = campo.getName();
							Criteria critTmp = criteria.createCriteria(path, JoinType.LEFT_OUTER_JOIN);
							if (!EntidadeUtils.isEntidadeVazia((EntidadeBasica) obj, true)) {
								critTmp.add(Example.create(obj).ignoreCase().setPropertySelector(selectorNaoBooleansNulos));
							}

                           /* Field chave = obj.getClass().getDeclaredField(key);
                            chave.setAccessible(true);
                            Object codigo = chave.get(obj);
                            if (codigo != null) {
                                if (!path.endsWith(campo.getName())) {
                                    path += (path.isEmpty() ? campo.getName() : (".") + campo.getName());
                                }
                                critTmp.add(Restrictions.eq(path + "." + key, codigo));
                            }*/
							criarExamples(critTmp, (T) obj, path);
						} else {
							if (campo.getName().equals(key)) {

								criteria.add(Restrictions.eq(key, obj));
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Page<E> pesquisar(E entidade, PageRequest paginacao) {
		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			criarExamples(criteria, entidade, "");

			return getPage(criteria, paginacao, null, null);
		} catch (Exception e) {
			EigLogger.error(e);
			return null;
		}
	}

	public Page<E> pesquisar(E entidade, PageRequest paginacao, FetchParametro... fetchs) {
		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			criarExamples(criteria, entidade, "");

			return getPage(criteria, paginacao, null, fetchs);
		} catch (Exception e) {
			e.printStackTrace();
			EigLogger.error(e);
			return null;
		}
	}

	public Page<E> pesquisar(E entidade, PageRequest paginacao, List<Order> orders, FetchParametro... fetchs) {
		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			criarExamples(criteria, entidade, "");

			return getPage(criteria, paginacao, orders, fetchs);
		} catch (Exception e) {
			EigLogger.error(e);
			return null;
		}
	}

	/**
	 * listar todos das entidade ordenados
	 *
	 * @param order
	 * @return
	 */
	public List<E> listar(Order order) {
		try {
			return getSession().createCriteria(getEntityClass()).addOrder(order).list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	/**
	 * Lista todas as entidades fazendo o fetch e ordenando
	 *
	 * @param orders
	 * @param fetchs
	 * @return
	 */
	public List<E> listar(List<Order> orders, FetchParametro... fetchs) {

		try {
			Criteria criteria = getSession().createCriteria(getEntityClass());
			setFetchs(criteria, fetchs);

			if (orders != null) {
				for (Order order : orders) {
					criteria.addOrder(order);
				}
			}

			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	/**
	 * filtra o campo pelo termo informado
	 *
	 * @param campo
	 * @param termo
	 * @return
	 */
	public List<E> autocomplete(String campo, String termo) {
		return autocomplete(campo, termo, null);
	}

	public <F extends EntidadeBasica> List<F> autocomplete(Class<F> clazz, String campo, String termo) {
		return autocomplete(clazz, campo, termo, null);
	}

	/**
	 * filtra o campo pelo termo informado e pelos parametros informados
	 *
	 * @param campo
	 * @param termo
	 * @param parametros
	 * @return
	 */
	public List<E> autocomplete(String campo, String termo, QueryParametro... parametros) {
		return autocomplete(null, campo, termo, parametros);
	}

	/**
	 * filtra o campo pelo termo informado e pelos parametros informados
	 *
	 * @param clazz
	 * @param campo
	 * @param termo
	 * @param parametros
	 * @return
	 */
	public <F extends EntidadeBasica> List<F> autocomplete(Class<F> clazz, String campo, String termo, QueryParametro... parametros) {
		try {
			this.entityClassAlternativo = clazz;
			Class<F> cl = clazz != null ? clazz : (Class<F>) entityClass;
			String strQuery = "select e from " + (getEntityClass().getName().substring(cl.getName().lastIndexOf(".") + 1, cl.getName().length())) + " e where upper(str(e." + campo + ")) like :campo ";

			if (parametros != null) {
				for (QueryParametro parametro : parametros) {
					strQuery += " and " + getHqlRestriction(parametro);
				}

			}
			strQuery += " order by e." + campo;

			Query query = entityManager.createQuery(strQuery);
			query.setParameter("campo", termo.toUpperCase() + "%");

			setQueryRestrictions(Arrays.asList(parametros), query);

			query.setFirstResult(0);
			query.setMaxResults(10);
			return query.getResultList();
		} catch (Exception e) {
			EigLogger.error(e);
			return Collections.emptyList();
		} finally {
			entityClassAlternativo = null;
		}
	}

	/**
	 * Atualiza a entidade atraves do {@link EntityManager#merge(Object)}
	 *
	 * @param entidade
	 * @return
	 */
	@Transactional
	public E atualizar(E entidade) {
		try {
			return entityManager.merge(entidade);
		} catch (Exception e) {
			EigLogger.error(e);
			return entidade;
		}
	}

	/**
	 * Exclui a entidade atraves do {@link EntityManager#remove(Object)}
	 *
	 * @param entidade
	 */
	@Transactional
	public void excluir(E entidade) {
		Method metodo = getAnnotatedMethod(PreRemove.class);
		if (metodo != null) {
			metodo.setAccessible(true);
			try {
				metodo.invoke(entidade);
			} catch (Exception e) {}
		}

		try {
			entidade = obterPorId(entidade.getCodigo());
			entityManager.remove(entidade);
		} catch (Exception e) {
			EigLogger.error(e);
		}
	}

	/**
	 * Exclui a entidade atraves do {@link EntityManager#remove(Object)}
	 *
	 * @param id
	 */
	@Transactional
	public void excluir(Serializable id) {
		E obj = obterPorId(id);
		excluir(obj);
	}

	/**
	 * Exclui as entidades pelos parametros
	 *
	 * @param parametros
	 */
	@Transactional
	public void excluir(List<QueryParametro> parametros) {
		try {
			String key = getKey(getEntityClass());
			if (key != null) {

				StringBuilder str = new StringBuilder();

				for (QueryParametro parametro : parametros) {
					str.append(" and ").
									append(getHqlRestriction(parametro));
				}

				String strQuery = "delete from " + (getEntityClass().getName().substring(getEntityClass().getName().lastIndexOf(".") + 1, getEntityClass().getName().length())) +
								" e where 1=1 " + str;

				Query query = entityManager.createQuery(strQuery);

				setQueryRestrictions(parametros, query);

				query.executeUpdate();

			}
		} catch (Exception e) {
			EigLogger.error(e);
		}
	}

	private void setQueryRestrictions(List<QueryParametro> parametros, Query query) {

		if (parametros != null && query != null) {
			for (QueryParametro parametro : parametros) {
				String atributo = parametro.getAtributo().replaceAll("\\.", "");
				if (parametro.isLikeSearch()) {
					if (parametro.getLikeMatchMode() != null) {
						String valor = parametro.isCaseSensitive() ? parametro.getValor().toString() : parametro.getValor().toString().toUpperCase();

						switch (parametro.getLikeMatchMode()) {
							case EXACT:
								query.setParameter(atributo, valor);
								break;
							case START:
								query.setParameter(atributo, "%" + valor);
								break;
							case END:
								query.setParameter(atributo, valor + "%");
								break;
							case ANYWHERE:
								query.setParameter(atributo, "%" + valor + "%");
								break;
						}
					}
				} else {
					if (parametro.getValor() != null) {
						query.setParameter(atributo, parametro.getValor());
					}
				}
			}
		}
	}

	private String getHqlRestriction(QueryParametro parametro) {
		String operador = parametro.isNegado() ? "!=" : "=";
		String atributo = "e." + parametro.getAtributo();

		Object valor = parametro.getValor();
		if (valor != null && valor instanceof String) {
			atributo = parametro.isCaseSensitive() ? parametro.getAtributo() : "upper(e." + parametro.getAtributo() + ")";
		} else if (valor == null) {
			operador = parametro.isNegado() ? "is not null" : "is null";
		} else {
			if (parametro.isMenor()) {
				operador = "<";
			}
			if (parametro.isMenorIgual()) {
				operador = "<=";
			}
			if (parametro.isMaior()) {
				operador = ">";
			}
			if (parametro.isMaiorIgual()) {
				operador = ">=";
			}
		}

		if (parametro.isLikeSearch()) {
			operador = parametro.isNegado() ? "not like" : "like";

		}
		return String.format("%s %s %s", atributo, operador, parametro.getValor() != null ? ":" + parametro.getAtributo().replaceAll("\\.", "") : "");
	}

	/**
	 * Inclui a entidade atraves do {@link EntityManager#persist(Object)}
	 * <p>
	 * depreciado: o hibernate fara o save automaticamente, chamar o persist so gera ciclos desperdicados
	 *
	 * @param entidade
	 */
	@Transactional
	public void inserir(E entidade) {
		entityManager.persist(entidade);
	}

	/**
	 * Inclui a entidade atraves do {@link Session#saveOrUpdate(Object)}
	 *
	 * @param entidade
	 */
	@Transactional
	public void inserirOuAtualizar(E entidade) {
		try {
			if (entidade.getCodigo() != null) {
				atualizar(entidade);
			} else {
				inserir(entidade);
			}
		} catch (Exception e) {
			EigLogger.error(e);
			throw e;
		}
	}

	/**
	 * Recuperar a entidade pelo atributo/valor
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @return
	 */
	protected E recuperarPorAtributo(String nomeAtributo, Object valorAtributo) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, null, null);
			return (E) criteria.uniqueResult();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return null;
		}
	}

	/**
	 * Recupera a entidade pelo atributo/valor e efetua os fetchs das entidades
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param fetchs
	 * @return
	 */
	protected E recuperarPorAtributo(String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, Arrays.asList(fetchs), null);
			setFetchs(criteria, fetchs);
			return (E) criteria.uniqueResult();
		} catch (Exception e) {
			EigLogger.error(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Recupera a entidade pelos atributos no {@link List<QueryParametro>}
	 *
	 * @param atributos
	 * @return
	 */
	protected E recuperarPorAtributo(List<QueryParametro> atributos) {
		try {
			Criteria criteria = getCriteriaMap(atributos, null, Collections.emptyList(), null);

			return (E) criteria.uniqueResult();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return null;
		}
	}

	protected E recuperarPorAtributo(List<QueryParametro> atributos, FetchParametro... fetchs) {
		try {
			for (QueryParametro atributo : atributos) {
				EigLogger.info(String.format("%s = %s", atributo.getAtributo(), atributo.getValor() != null ? atributo.getValor().toString() : ""));
			}

			Criteria criteria = getCriteriaMap(atributos, null, Arrays.asList(fetchs), null);
			setFetchs(criteria, fetchs);
			return (E) criteria.uniqueResult();
		} catch (Exception e) {
			EigLogger.error(e);
			return null;
		}
	}

	/**
	 * Lista todas as entidades pelo atributo/valor
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @return
	 */
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, null, null);
			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	/**
	 * Lista todas as entidades pelo atributo/valor com o {@link JoinType} especificado
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @return
	 */
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, JoinType joinType) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), joinType, null, null);
			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	private Criteria getCriteriaAtributo(String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		Criteria criteria = getSession().createCriteria(getEntityClass());
		criteria.add(Restrictions.eq(nomeAtributo, valorAtributo));

		setFetchs(criteria, fetchs);

		return criteria;
	}

	private void setFetchs(Criteria criteria, FetchParametro[] fetchs) {
		if (fetchs != null) {
			for (FetchParametro parametro : fetchs) {

				String key = parametro.getAtributo();

				if (key.contains(".")) {
					//					String novaKey = key.substring(0, key.lastIndexOf("."));

					String[] alias = key.split("\\.");
					String tmpKey = "";
					for (String s : alias) {
						if (tmpKey.isEmpty()) {
							tmpKey = s;
						} else {
							tmpKey += "." + s;
						}
						//						criteria.createAlias(tmpKey, s, parametro.getJoinType());
						criteria.setFetchMode(tmpKey, parametro.getFetchMode());
					}
				} else {
					criteria.createAlias(key, key, parametro.getJoinType());
					criteria.setFetchMode(key, parametro.getFetchMode());
				}
			}
		}
	}

	/**
	 * Lista todas as entidades pelo atributo/valor e efetua o fetch das entidades
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param fetchs
	 * @return
	 */
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, Arrays.asList(fetchs), null);
			setFetchs(criteria, fetchs);

			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	/**
	 * Lista todas as entidades pelo atributo/valor e efetua o fetch das entidades
	 * fazendo a paginacao...
	 *
	 * @param paginacao
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param fetchs
	 * @return
	 */
	protected Page<E> listarPorAtributo(PageRequest paginacao, String nomeAtributo, Object
					valorAtributo, FetchParametro... fetchs) {
		try {
			Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, Arrays.asList(fetchs), null);
			setFetchs(criteria, fetchs);

			return getPage(criteria, paginacao, null, null);
		} catch (Exception e) {
			EigLogger.error(e);
			return null;
		}
	}

	/**
	 * Lista as entidades filtrando pelo {@link List<QueryParametro>}
	 *
	 * @param atributos
	 * @return
	 */
	public List<E> listarPorAtributo(QueryParametro... atributos) {
		return listarPorAtributo(Arrays.asList(atributos), null, new Order[] {});
	}

	public List<E> listarPorAtributo(List<QueryParametro> parametros, Order... order) {
		return listarPorAtributo(parametros, null, order);
	}

	protected List<E> listarPorAtributo(List<QueryParametro> atributos, JoinType joinType) {
		return listarPorAtributo(atributos, null, new Order[] {});
	}

	/**
	 * Lista as entidades filtrando pela {@link List<QueryParametro>} e orderna
	 *
	 * @param atributos
	 * @param order
	 * @return
	 */
	protected List<E> listarPorAtributo(List<QueryParametro> atributos, JoinType joinType, Order... order) {
		try {
			Criteria criteria = getCriteriaMap(atributos, joinType, Collections.emptyList(), null);
			for (Order ordem : order) {
				criteria.addOrder(ordem);
			}
			return criteria.list();
		} catch (HibernateException e) {
			EigLogger.error(e);
			return Collections.emptyList();
		}
	}

	/**
	 * Lista as entidades filtrando pela {@link List<QueryParametro>} e orderna
	 *
	 * @param atributos
	 * @param maxResult
	 * @param order
	 * @return
	 */
	protected List<E> listarPorAtributo(List<QueryParametro> atributos, JoinType joinType, Integer
					maxResult, Order... order) {
		Criteria criteria = getCriteriaMap(atributos, joinType, Collections.emptyList(), maxResult);
		for (Order ordem : order) {
			criteria.addOrder(ordem);
		}
		return criteria.list();
	}

	/**
	 * Lista as entidades filtrando pela {@link List<QueryParametro>}  e faz o fetch das entidades
	 *
	 * @param atributos
	 * @param fetchs
	 * @return
	 */
	protected List<E> listarPorAtributo(List<QueryParametro> atributos, FetchParametro... fetchs) {
		Criteria criteria = getCriteriaMap(atributos, null, fetchs != null ? Arrays.asList(fetchs) : Collections.emptyList(), null);
		setFetchs(criteria, fetchs);
		return criteria.list();
	}

	/**
	 * Lista as entidades filtrando pelo {@link List<QueryParametro>}, fazendo o join das tabelas pelo {@link JoinType} e faz o fetch das entidades e ordena
	 *
	 * @param atributos
	 * @param orders
	 * @param fetchs
	 * @return
	 */
	protected List<E> listarPorAtributo(List<QueryParametro> atributos, List<Order> orders, FetchParametro...
					fetchs) {
		Criteria criteria = getCriteriaMap(atributos, null, fetchs != null ? Arrays.asList(fetchs) : Collections.emptyList(), null);

		for (Order order : orders) {
			criteria.addOrder(order);
		}

		setFetchs(criteria, fetchs);

		return criteria.list();
	}

	protected Page<E> listarPorAtributo(PageRequest
					paginacao, List<QueryParametro> atributos, List<Order> orders, FetchParametro... fetchs) {
		Criteria criteria = getCriteriaMap(atributos, null, fetchs != null ? Arrays.asList(fetchs) : Collections.emptyList(), null);

		return getPage(criteria, paginacao, orders, null);
	}

	public <F extends EntidadeBasica> Page<F> listarPorAtributo(Class<F> clazz, PageRequest
					paginacao, List<QueryParametro> atributos, List<Order> orders, FetchParametro... fetchs) {
		try {
			entityClassAlternativo = clazz;
			Criteria criteria = getCriteriaMap(atributos, null, fetchs != null ? Arrays.asList(fetchs) : Collections.emptyList(), null);

			Page<F> page = (Page<F>) getPage(criteria, paginacao, orders, null);

			return page;
		} catch (Exception e) {
			e.printStackTrace();
			EigLogger.error(e);
		} finally {
			entityClassAlternativo = null;
		}
		return null;
	}

	/**
	 * Lista as entidades pelo atributo/valor e as ordena
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param order
	 * @return
	 */
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, Order... order) {
		Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, null, null);
		for (Order ordem : order) {
			criteria.addOrder(ordem);
		}
		return criteria.list();
	}

	/**
	 * Lista as entidades pelo atributo/valor e as ordena
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param maxResult
	 * @param order
	 * @return
	 */
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, Integer maxResult, Order... order) {
		Criteria criteria = getCriteriaMap(Arrays.asList(QueryParametro.getAtributo(nomeAtributo, valorAtributo)), null, null, maxResult);
		for (Order ordem : order) {
			criteria.addOrder(ordem);
		}
		return criteria.list();
	}

	private Criteria getCriteriaMap(List<QueryParametro> atributos, JoinType
					joinType, List<FetchParametro> fetchs, Integer maxResult) {
		Criteria criteria = getSession().createCriteria(getEntityClass());
		criteria.setCacheable(false);

		if (maxResult != null) {
			criteria.setMaxResults(maxResult);
		}

		for (QueryParametro parametro : atributos) {
			Object value = parametro.getValor();
			String chaveValor = parametro.getAtributo();
			String key = parametro.getAtributo();
			if (key.contains(".")) {
				String[] alias = key.split("\\.");
				String novaKey = key.substring(0, key.lastIndexOf("."));
				chaveValor = alias[alias.length - 1];

				alias = novaKey.split("\\.");
				String tmpKey = "";
				for (String s : alias) {
					if (tmpKey.isEmpty()) {
						tmpKey = s;
					} else {
						tmpKey += "." + s;
					}
					JoinType jj = null;
					if (joinType == null) {
						jj = getJoinType(fetchs, parametro);
						jj = jj != null ? jj : JoinType.INNER_JOIN;
					}
					criteria.createAlias(tmpKey, s, joinType != null ? joinType : jj);
				}

				chaveValor = alias[alias.length - 1] + "." + chaveValor;
			}
			if (value != null && !value.toString().isEmpty()) {
				if (value instanceof String) {
					if (parametro.isLikeSearch()) {
						criteria.add(getLikeRestriction(chaveValor, parametro));
					} else {
						Criterion eq = getRestriction(chaveValor, parametro);
						if (!parametro.isCaseSensitive()) {
							((SimpleExpression) eq).ignoreCase();
						}
						if (parametro.isNegado()) {
							criteria.add(Restrictions.not(eq));
						} else {
							criteria.add(eq);
						}
					}
				} else if (value instanceof Collection || value instanceof Arrays) {
					Criterion in = Restrictions.in(chaveValor, (Collection) value);
					if (parametro.isNegado()) {
						criteria.add(Restrictions.not(in));
					} else {
						criteria.add(in);
					}
				} else {
					Criterion eq = getRestriction(chaveValor, parametro);

					if (parametro.isNegado()) {
						criteria.add(Restrictions.not(eq));
					} else {
						criteria.add(eq);
					}
				}
			}
		}

    /*    if (fetchs != null) {
            for (FetchParametro fetch : fetchs) {
                criteria.setFetchMode(fetch.getAtributo(), fetch.getFetchMode());
            }
        }*/

		return criteria;
	}

	private JoinType getJoinType(List<FetchParametro> fetchs, final QueryParametro parametro) {
		if (fetchs == null) {
			return null;
		}
		if (!fetchs.isEmpty()) {
			Optional<FetchParametro> any = fetchs.stream().filter(fetch -> fetch.getAtributo().equals(parametro.getAtributo())).findAny();
			if (any.isPresent()) {
				return any.get().getJoinType();
			} else {
				return null;
			}

		}
		return null;
	}

	private Criterion getLikeRestriction(String chaveValor, QueryParametro queryParametro) {
		Criterion like;
		String valor = queryParametro.getValor().toString();
		if (queryParametro.isCaseSensitive()) {
			like = Restrictions.like(chaveValor, valor, queryParametro.getLikeMatchMode());
		} else {
			like = Restrictions.ilike(chaveValor, valor, queryParametro.getLikeMatchMode());
		}
		if (queryParametro.isNegado()) {
			return Restrictions.not(like);
		}
		return like;
	}

	private Criterion getRestriction(String chaveValor, QueryParametro queryParametro) {
		if (queryParametro.isMaior()) {
			return Restrictions.gt(chaveValor, queryParametro.getValor());
		}

		if (queryParametro.isMaiorIgual()) {
			return Restrictions.ge(chaveValor, queryParametro.getValor());
		}

		if (queryParametro.isMenor()) {
			return Restrictions.lt(chaveValor, queryParametro.getValor());
		}

		if (queryParametro.isMenorIgual()) {
			return Restrictions.le(chaveValor, queryParametro.getValor());
		}

		return Restrictions.eqOrIsNull(chaveValor, queryParametro.getValor());
	}

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	private Method getAnnotatedMethod(Class annotation) {
		Method[] declaredMethods = getEntityClass().getDeclaredMethods();
		for (Method declaredMethod : declaredMethods) {
			if (declaredMethod.isAnnotationPresent(annotation)) {
				return declaredMethod;
			}
		}
		return null;
	}

	public List<?> queryNativaList(NativeQueryParametro nativeQueryParametro) {
		try {
			return (getNativeQueryExecutor(nativeQueryParametro)).list();
		} catch (NoResultException noResult) {
			EigLogger.info(noResult);
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public Object queryNativaUniqueResult(NativeQueryParametro nativeQueryParametro) {
		try {
			return (getNativeQueryExecutor(nativeQueryParametro)).uniqueResult();
		} catch (NoResultException noResult) {
			EigLogger.info(noResult);
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Transactional
	public void queryNativaUpdate(NativeQueryParametro nativeQueryParametro) {
		try {
			(getNativeQueryExecutor(nativeQueryParametro)).executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param nativeQueryParametro
	 * @return
	 * @see 'Utilizado para retornar query nativa;
	 * O cada retorno da query deve possuir um alias equivalente em sua entidade e seus respectivos geter e setters.'
	 */
	private org.hibernate.Query getNativeQueryExecutor(NativeQueryParametro nativeQueryParametro) {
		StringBuilder log = new StringBuilder();
		try {
			String sqlQuery = nativeQueryParametro.getMap().get(1).toString();
			Integer maxResult = (Integer) nativeQueryParametro.getMap().get(2);
			List<QueryParametro> parametros = (List<QueryParametro>) nativeQueryParametro.getMap().get(3);
			List<String> replace = (List<String>) nativeQueryParametro.getMap().get(4);
			Class classe = (Class) nativeQueryParametro.getMap().get(5);
			List<Object[]> referenciaColunaRetorno = (List<Object[]>) nativeQueryParametro.getMap().get(6);
			List<Object[]> parametrosComReplace = (List<Object[]>) nativeQueryParametro.getMap().get(7);
			List<Object[]> queryParametroList = (List<Object[]>) nativeQueryParametro.getMap().get(8);

			if (sqlQuery.toUpperCase().startsWith("INSERT")) {
				log.append("Query insert detectada. \n");
				throw new RuntimeException("ESTE METODO NAO FAZ INSERT.");
			} else if (sqlQuery.replaceAll(" ", "").toUpperCase().startsWith("UPDATE") && !sqlQuery.toUpperCase().contains("WHERE")) {
				log.append("Update sem where detectado. \n");
				throw new RuntimeException("####### ERRO: UPDATE SEM WHERE ####");
			} else {
				String nativeSql = replaceQueryParameter((replaceNativeQueryParameter(sqlQuery, replace)), parametrosComReplace);

				List<QueryParametro> remover = new ArrayList<>();
				List<QueryParametro> adicionar = new ArrayList<>();

				if (parametrosComReplace != null && !parametrosComReplace.isEmpty()) {
					for (Object[] param : parametrosComReplace) {
						parametros.add(QueryParametro.getAtributo(((QueryParametro) param[1]).getAtributo(), ((QueryParametro) param[1]).getValor()));
					}
				}

				for (QueryParametro p : parametros) {
					if (p.getValor() instanceof ArrayList && p.getTipoCampoNativo() == null) {
						List<QueryParametro> atributos = new ArrayList<>();

						int incremento = 0;
						for (int i = 0; i < ((ArrayList) p.getValor()).size(); i++) {
							StringBuilder atributo = new StringBuilder()
											.append(p.getAtributo())
											.append("_")
											.append(RandomStringUtils.randomAlphanumeric(6).toUpperCase())
											.append("_")
											.append(incremento++);

							atributos.add(QueryParametro.getAtributo(atributo.toString(), ((ArrayList) p.getValor()).get(i)));
						}

						if (!atributos.isEmpty()) {

							int count = 0;
							int mapKey = 0;
							StringBuilder replaceAux = new StringBuilder();
							Map<Integer, String> mapReplace = new HashMap<>();
							for (int i = 0; i < atributos.size(); i++) {
								count++;
								replaceAux.append(String.format(":%s", atributos.get(i).getAtributo()));
								if (count <= limiteClausulaIn && i + 1 != atributos.size()) {
									replaceAux.append(",");
								} else {
									count = 0;
									mapReplace.put(mapKey++, replaceAux.toString());
									replaceAux = new StringBuilder();
								}
							}

							StringBuilder finalReplace = new StringBuilder();
							for (int i = 0; i < mapReplace.size(); i++) {
								if (i != 0) {
									String[] sqlPartido = (nativeSql.substring(0, nativeSql.indexOf(String.format(":%s", p.getAtributo())))).split(" ");
									int lenghRemov = 2;
									if ((sqlPartido[sqlPartido.length - 2]).equalsIgnoreCase("in")) {
										lenghRemov = 3;
									}
									String atributo = sqlPartido[sqlPartido.length - lenghRemov];
									StringBuilder in = new StringBuilder(" OR ")
													.append(atributo)
													.append(" in ");
									finalReplace.append(in);
								}
								finalReplace.append(String.format("( %s )", mapReplace.get(i)));
							}
							remover.add(p);
							adicionar.addAll(atributos);

							nativeSql = nativeSql.replace(String.format(":%s", p.getAtributo()), finalReplace.toString());
						}
					}
				}

				if (!remover.isEmpty()) {
					parametros.removeAll(remover);
				}
				if (!adicionar.isEmpty()) {
					parametros.addAll(adicionar);
				}

				org.hibernate.Query query = getSession().createSQLQuery(nativeSql);
				if (classe != null) {
					query.setResultTransformer(Transformers.aliasToBean(classe));
				} else {
					query.setResultTransformer(Transformers.aliasToBean(this.getEntityClass()));
				}

				if (parametros != null && !parametros.isEmpty()) {
					for (QueryParametro parametro : parametros) {
						if (nativeSql.contains(parametro.getAtributo())) {
							if (parametro.getValor() != null && parametro.getTipoCampoNativo() != null) {
								if (parametro.getValor() instanceof ArrayList) {
									query.setParameterList(parametro.getAtributo(), ((ArrayList) parametro.getValor()).toArray(), parametro.getTipoCampoNativo().getType());
								} else {
									query.setParameter(parametro.getAtributo(), parametro.getValor(), parametro.getTipoCampoNativo().getType());
								}

							} else {
								query.setParameter(parametro.getAtributo(), parametro.getValor());
							}
						} else {
							log.append(String.format(" Query nao possui o atributo informado ( %s ). \n", parametro.getAtributo()));
						}
					}
				}

				if (queryParametroList != null && !queryParametroList.isEmpty()) {
					for (Object[] qp : queryParametroList) {
						query.setParameterList(((QueryParametro) qp[0]).getAtributo(), (Object[]) ((QueryParametro) qp[0]).getValor(), ((TipoRetornoNativeQueryEnum) qp[1]).getType());
					}
					EigLogger.info(nativeSql.toString());
				}

				if (referenciaColunaRetorno != null && !referenciaColunaRetorno.isEmpty()) {
					for (Object[] referencia : referenciaColunaRetorno) {
						((SQLQuery) query).addScalar((String) referencia[0], ((TipoRetornoNativeQueryEnum) referencia[1]).getType());
					}
				} else {
					log.append("\n Nao foram encontrados referencias de retorno.\n");
				}

				if (maxResult != null) {
					query.setMaxResults(maxResult);
				}
				return query;
			}
		} catch (Exception e) {
			EigLogger.error("\n\n\n ##### Erro ao criar objeto Hibernate Query");
			throw e;
		} finally {
			if (!log.toString().isEmpty()) {
				EigLogger.info("\n\n\n\n ##NativeQueryLog: \n\n" + log.toString() + "\n\n\n\n\n");
			}
		}
	}

	private String replaceNativeQueryParameter(String sql, List<String> atributos) {
		if (sql != null && atributos != null && !atributos.isEmpty()) {
			StringBuilder sqlAux = new StringBuilder(sql);
			try {
				for (int i = 0; i < atributos.size(); i++) {
					//noinspection
					sqlAux = new StringBuilder(sqlAux.toString().replaceFirst("\\?", String.format(":%s", atributos.get(i))));
				}
				return sqlAux.toString();
			} catch (Exception e) {
				EigLogger.error(e);
			}
		}
		return sql;
	}

	private String replaceQueryParameter(String sql, List<Object[]> parametrosComReplace) {
		if (sql != null && parametrosComReplace != null && !parametrosComReplace.isEmpty()) {
			StringBuilder sqlBuilded = new StringBuilder();
			int totalEntroncado = 0;
			Map<Integer, String> replaced = new HashMap<>();
			for (int i = 0; i < sql.length(); i++) {
				if ('?' == sql.charAt(i)) {
					totalEntroncado++;
					for (Object[] paramReplace : parametrosComReplace) {
						int posicao = (Integer) paramReplace[0];
						QueryParametro queryParametro = (QueryParametro) paramReplace[1];
						if (totalEntroncado == posicao) {
							replaced.put(posicao, queryParametro.getAtributo());
							sqlBuilded.append(String.format(" :%s", queryParametro.getAtributo()));
						}

					}
				} else {
					sqlBuilded.append(sql.charAt(i));
				}
			}
			if (totalEntroncado == replaced.size()) {
				return sqlBuilded.toString();
			} else {
				StringBuilder sb = null;
				for (Object[] paramReplace : parametrosComReplace) {
					QueryParametro queryParametro = (QueryParametro) paramReplace[1];
					int posicao = (Integer) paramReplace[0];
					if (!replaced.containsKey(posicao)) {
						if (sb == null) {
							sb = new StringBuilder();
							sb.append(" PROBLEMAS IDENTIFICADOS:  \n\n\n");
						}
						sb.append(String.format("Atributo : %s, nao econtrado. \n", queryParametro.getAtributo()))
										.append(String.format("Posicao informada: %s \n", posicao));
					}
				}
				if (sb != null && !sb.toString().isEmpty()) {
					throw new RuntimeException(String.format("ERRO DURANTE REPLACE DE ALGUNS ATRIBUTOS. \n\n %s", sb.toString()));
				}
			}
		}
		return sql;
	}

	private Page<E> getPage(Criteria criteria, PageRequest paginacao, List<Order> orders, FetchParametro...
					fetchs) {
		Page<E> page;

		criteria.setProjection(Projections.rowCount());
		Long total = (Long) criteria.uniqueResult();
		if (total > 0) {
			criteria.setProjection(null);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

			if (fetchs != null && fetchs.length > 0) {
				setFetchs(criteria, fetchs);
			}

			if (orders != null) {
				for (Order order : orders) {
					criteria.addOrder(order);
				}
			}

			criteria.setMaxResults(paginacao.getPageSize() > 0 ? paginacao.getPageSize() : 10);
			criteria.setFirstResult(paginacao.getPageNumber());
			page = new PageImpl<>(criteria.list(), paginacao, total);
		} else {
			page = new PageImpl<>(Collections.emptyList(), paginacao, total);
		}

		return page;
	}

	protected Example.PropertySelector selectorNaoBooleansNulos = (valor, campo, tipo) -> {
		if (valor == null) {
			return false;
		}

		if (tipo.getClass().equals(BooleanType.class) && !(boolean) valor) {
			return false;
		}

		return true;
	};

}