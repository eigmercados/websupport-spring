/*
 * Copyright @ Ministerio da Saude.
 *
 * Este software e confidencial e de propriedade do Ministerio da Saude.
 * Nao e permitida sua distribuicao ou divulgacao do seu conteudo sem 
 * expressa autorizaao do mesmo.
 */
package br.com.eigmercados.websupport.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Deserializador que deve ser usado em todas as entidades que seram recebidas
 * atraves de uma requisição REST nas controllers da aplicação.
 * Ele trata o formato padrao de data  recebidos na requisição
 */
public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {


    @Override
    public LocalTime deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {

        LocalTime retorno = null;
        try {
            retorno = LocalTime.from(DateTimeFormatter.ISO_TIME.parse(jsonparser.getText()));
        } catch (Exception e) {

        }

        return retorno;
    }
}