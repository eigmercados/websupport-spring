package br.com.eigmercados.websupport.converters;

import br.com.eigmercados.websupport.model.RegistroAtivo;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RegistroAtivoConverter implements AttributeConverter<RegistroAtivo, Boolean> {

	@Override
	public Boolean convertToDatabaseColumn(RegistroAtivo registroAtivo) {
		if (registroAtivo != null) {
			return RegistroAtivo.ATIVO == registroAtivo;
		}
		return false;
	}

	@Override
	public RegistroAtivo convertToEntityAttribute(Boolean codigoSituacao) {
		return codigoSituacao ? RegistroAtivo.ATIVO : RegistroAtivo.INATIVO;
	}

}
