package br.com.eigmercados.websupport.configuracao.security;

public enum FuncionalidadeEnum {

    SIST(E.SIST),
    FUNC(E.FUNC),
    USU(E.USU),
    PERF(E.PERF),
    ADM(E.ADM),
    PESS(E.PESS),
    MENU(E.MENU);

    private String sigla;

    FuncionalidadeEnum(String sigla) {
        this.sigla = sigla;
    }

    public String getSigla() {
        return sigla;
    }

    public static class E {

        public final static String SIST = "SIST";
        public final static String USU = "USU";
        public final static String PERF = "PERF";
        public final static String ADM = "ADM";
        public final static String FUNC = "FUNC";
        public final static String PESS = "PESS";
        public final static String MENU = "MENU";
    }

}

