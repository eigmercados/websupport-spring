package br.com.eigmercados.websupport.utils;

import br.com.eigmercados.websupport.vos.MensagemEmailVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class EmailListener {

	protected final Log logger = LogFactory.getLog(getClass());

	private final Long TIMEOUT = 1000l;

	//    @Resource(lookup = "java:jboss/mail/EigMailSender")
	//    private Session session;

	@Autowired
	private JavaMailSender emailSender;

	private MensagemEmailVO mensagemEmailVO;

	//    @Async
	@EventListener
	public void enviarEmail(EmailEvent emailEvent) {
		this.mensagemEmailVO = (MensagemEmailVO) emailEvent.getSource();
		try {
			/*Properties prop = new Properties();

			prop.put("mail.smtp.timeout", TIMEOUT.toString());
			prop.put("mail.smtp.connectiontimeout", TIMEOUT.toString());


			Message message = new MimeMessage(session.getInstance(prop));
			message.setSubject(mensagemEmailVO.getAssunto());
			message.setText(mensagemEmailVO.getMensagem());

			List<Address> addresses = new ArrayList<>();
			mensagemEmailVO.getDestinatarios().forEach(dest -> {
				try {
					InternetAddress e = new InternetAddress(dest);
					e.setPersonal("pulguento");
					addresses.add(e);
				} catch (AddressException | UnsupportedEncodingException e) {

				}
			});

			Address[] tmp = new Address[addresses.size()];
			message.setRecipients(Message.RecipientType.TO, addresses.toArray(tmp));

			Transport.send(message);*/

			Properties prop = new Properties();

			prop.put("mail.smtp.timeout", TIMEOUT.toString());
			prop.put("mail.smtp.connectiontimeout", TIMEOUT.toString());

			SimpleMailMessage message = new SimpleMailMessage();

			message.setSubject(mensagemEmailVO.getAssunto());
			message.setText(mensagemEmailVO.getMensagem());

			message.setTo(mensagemEmailVO.getDestinatarios().toArray(new String[] {}));

			emailEvent.setEnviado(true);

		} catch (Exception e) {
			logger.error(e);
			emailEvent.setMensagemErro(e.getMessage());
			emailEvent.setEnviado(false);
		}
	}
}
