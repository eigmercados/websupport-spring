/*
* Copyright @ Ministério da Saúde.
*
* Este software é confidencial e de propriedade do Ministério da Saúde.
* Não é permitida sua distribuição ou divulgação do seu conteúdo sem 
* expressa autorização do mesmo.
*/
package br.com.eigmercados.websupport.geral.i18n;


/**
 * Interface utilizada pelos domínios de mensagem {@link Enum} com intuito de realizar
 * a internacionalização destas. Ex.:
 * 
 * <pre>
 * {@code
 * 
 * public enum MensagemEnum implements I18N {
 * 	MSG001;	
 * 
 *	public String chave() {
 *		return this.name();
 *	}
 * 
 * }
 * 
 * </pre>
 *
 *
 */
public interface I18N {

	/**
	 * Chave da mensagem contida no arquivo de propriedade
	 * @return Retorna a chave da mensagem
	 */
	String chave();
}
