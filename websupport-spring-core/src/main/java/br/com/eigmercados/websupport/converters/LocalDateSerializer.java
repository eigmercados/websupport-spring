package br.com.eigmercados.websupport.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Serializador que altera o formato padrão que as datas deveram ser entregues ao cliente.
 *
 */
public class LocalDateSerializer extends JsonSerializer<LocalDate> {
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public void serialize(LocalDate date, JsonGenerator gen, SerializerProvider provider) throws IOException {
        try {
            //converto o localDate para localDateTime, pois o construtor do javascript aceita a data neste formato.
            gen.writeString(date.atStartOfDay().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        } catch (Exception e) {
            logger.debug(e.getMessage());
            gen.writeString("");
        }
    }
}