package br.com.eigmercados.websupport.utils;

import br.com.eigmercados.websupport.vos.MensagemEmailVO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class EmailEvent extends ApplicationEvent {
    private boolean enviado;
    private String mensagemErro;

    public EmailEvent(MensagemEmailVO source) {
        super(source);
    }
}

