package br.com.eigmercados.websupport.model;

import br.com.eigmercados.websupport.estereotipo.Convertable;
import br.com.eigmercados.websupport.estereotipo.JpaParam;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.Hibernate;
import org.hibernate.LazyInitializationException;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@MappedSuperclass
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class EntidadeBasica implements Serializable {

    private static final long serialVersionUID = 7086808939746169469L;

    private transient String uniqueName;

    public abstract Serializable getCodigo();

    @SuppressWarnings("rawtypes")
    static final Map<Class, Method[]> cacheIdMethods = new LinkedHashMap<Class, Method[]>();

    {
        if (!cacheIdMethods.containsKey(this.getClass())) {
            synchronized (cacheIdMethods) {
                if (!cacheIdMethods.containsKey(this.getClass())) {
                    List<Method> idMethods = new ArrayList<Method>();

                    Method[] methods = getClass().getMethods();
                    for (Method method : methods) {
                        if (method.isAnnotationPresent(Id.class) || method.isAnnotationPresent(EmbeddedId.class))
                            idMethods.add(method);
                    }

                    cacheIdMethods.put(this.getClass(), idMethods.toArray(new Method[] {}));
                }
            }
        }
    }

    /**
     * Indica se a PK mapeada é composta
     */
    @Transient
    public Boolean getCompositeId() {
        Method[] ms = cacheIdMethods.get(this.getClass());
        return (ms != null && ms.length > 1);
    }

    @Transient
    private static boolean isID(Field campo) {
        return campo.isAnnotationPresent(Id.class);
    }

    /**
     * Retorna o toString() do Id mapeado em JPA 2 conforme o formato especificado
     */
    public String toStringMappedId(ToStringStyle style) {
        try {
            ToStringBuilder builder = new ToStringBuilder(this, style);

            for (Method m : cacheIdMethods.get(getClass())) {
                String name = m.getName();
                if (name.startsWith("get"))
                    name = name.substring(3);

                builder.append(name, m.invoke(this, (Object[]) null));
            }

            return builder.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retorna o toString() do Id mapeado em JPA 2 no formato:
     *
     * <pre>
     * John Doe,33,false
     * </pre>
     */
    public String toStringMappedId() {
        return toStringMappedId(ToStringStyle.SIMPLE_STYLE);
    }

    /**
     * Retorna o toString() do Id mapeado em JPA 2 no formato:
     *
     * <pre>
     * Person[name=John Doe,age=33,smoker=false]
     * </pre>
     */
    @Override
    public String toString() {
        return toStringMappedId(ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int hashCode() {
        try {
            final int prime = 31;
            int result = 1;

            for (Method m : cacheIdMethods.get(getClass())) {
                Object obj = m.invoke(this, (Object[]) null);
                if (obj != null) {
                    int tempHashCode = obj.hashCode();
                    result = prime * result + tempHashCode;
                }
            }

            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Resolve a questão do 'instanceof/isAssignableFrom' devido ao proxy do Hibernate.
     */
    @Override
    public boolean equals(Object obj) {
        try {
            if (obj == null) {
                return false;
            }
            if ((this == obj)) {
                return true;
            }

            // resolve comparacao entre entidades transientes e entidades com proxy
            Class<?> c1 = Hibernate.getClass(this);
            Class<?> c2 = Hibernate.getClass(obj);

            if (!c1.equals(c2)) {
                return false;
            }

            for (Method m : cacheIdMethods.get(getClass())) {
                Object myValue = m.invoke(this, (Object[]) null);
                Object otherValue = m.invoke(obj, (Object[]) null);

                if (myValue != null && otherValue == null)
                    return false;

                if (myValue == null && otherValue != null)
                    return false;

                if (myValue != null && otherValue != null) {
                    if (!myValue.equals(otherValue))
                        return false;
                }
            }

            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    @Transient
    public String getUniqueName() {
        if (this.uniqueName == null) {
            try {
                this.uniqueName = this.toString();
            } catch (Exception e) {
                if (getCodigo() != null) {
                    this.uniqueName = this.getClass().getName() + "@@" + getCodigo().hashCode();
                } else {
                    this.uniqueName = this.getClass().getName() + "@@@";
                }
            }
        }
        return this.uniqueName;
    }

    private void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    /**
     * Converte a entidade no objeto da Classe especificada
     *
     * @param classObjeto
     * @return
     */
    public <E extends Convertable> E converter(Class<E> classObjeto) {
        if (classObjeto != null) {
            try {
                E novo = (E) classObjeto.newInstance();

                Field[] declaredFields = classObjeto.getDeclaredFields();

                for (Field fieldOrigem : declaredFields) {
                    JpaParam jpaParam = getJpaAnnotation(fieldOrigem);
                    if (jpaParam != null) {
                        try {
                            List<Object> listaTmp = new ArrayList<>();
                            Object valorTmp = getValor(this, jpaParam, listaTmp);
                            if (valorTmp != null && listaTmp.isEmpty()) {
                                listaTmp.add(valorTmp);
                            }
                            if (!listaTmp.isEmpty()) {
                                Object valor = processaCampo(fieldOrigem, jpaParam, listaTmp.size() > 1 || valorTmp instanceof Collection ? listaTmp : listaTmp.get(0));
                                if (valor != null) {
                                    //TODO: QUANDO EH EMBEDDED, O VALOR NAO ESTA SENDO RECUPERADO QUANDO O ID FINAL NAO TEM O MESMO NOME DO CAMPO
                                    if (isID(fieldOrigem) && ((Number) valor).intValue() == 0) {
                                        valor = null;
                                    }
                                    setaValor(valor, novo, fieldOrigem.getName());
                                }
                            } else {
                                if (jpaParam.vazioParaNull()) {
                                    setaValor("", novo, fieldOrigem.getName());
                                }
                            }

                        } catch (Exception e) {
                            //							e.printStackTrace();
                        }
                    }
                }

                return novo;

            } catch (InstantiationException | IllegalAccessException e) {
                //				e.printStackTrace();
            }
        }
        return null;
    }

    private static Object processaCampo(Field fieldOrigem, JpaParam jpaParam, Object valor) {
        if (valor instanceof EntidadeBasica && !jpaParam.converterPara().equals(Convertable.class)) {
            valor = ((EntidadeBasica) valor).converter(jpaParam.converterPara());
        } else if (valor instanceof Collection && !jpaParam.converterPara().equals(Convertable.class)) {
            Collection lista = (Collection) valor;
            Set novaLista = new HashSet();
            for (Object o : lista) {
                try {
                    novaLista.add(((EntidadeBasica) o).converter(jpaParam.converterPara()));
                } catch (Exception e) {
                    //					e.printStackTrace();
                }
            }
            if (fieldOrigem.getType().equals(List.class)) {
                valor = new ArrayList<>(novaLista);
            } else {
                valor = novaLista;
            }
        }
        return valor;
    }

    private static Object getValor(Object entidade, String parametro)
                    throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException {

        Queue<String> parametros = new ArrayDeque<>(Arrays.asList(parametro.split("\\.")));
        return getValor(entidade, null, parametros, null);
    }

    private static Object getValor(Object entidade, JpaParam parametro, List<Object> listaTmp)
                    throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException {

        Queue<String> parametros = new ArrayDeque<>(Arrays.asList(parametro.value().split("\\.")));
        return getValor(entidade, parametro, parametros, listaTmp);
    }

    private static Object getValor(Object entidade, JpaParam parametro, Queue<String> parametros, List<Object> listaTmp)
                    throws InvocationTargetException, IllegalAccessException, NoSuchFieldException {

        String param = parametros.poll();
        if (param != null) {
            String prefixo = "get";
            Field campoField = entidade.getClass().getDeclaredField(param);
            Class<?> tipo = campoField.getType();
            if (tipo.isAssignableFrom(boolean.class)) {
                prefixo = "is";
            }
            try {

                Object tmp;
                try {
                    tmp = entidade.getClass().getDeclaredMethod(prefixo + StringUtils.capitalize(param)).invoke(entidade);
                } catch (NoSuchMethodException e) {
                    try {
                        tmp = entidade.getClass().getDeclaredMethod(prefixo + param).invoke(entidade);
                    } catch (NoSuchMethodException e1) {
                        Field fi = entidade.getClass().getDeclaredField(param);
                        fi.setAccessible(true);
                        tmp = fi.get(entidade);
                    }
                }

                if (tmp instanceof Collection) {
                    if (!((Collection) tmp).isEmpty()) {
                        if (parametro.primeiroDaLista()) {
                            if (parametro != null && parametro.converterPara().equals(Convertable.class)) {
                                tmp = ((Collection) tmp).iterator().next();
                                if (tmp instanceof EntidadeBasica) {
                                    return getValor(tmp, parametro, parametros, listaTmp);
                                }
                            }
                        } else {
                            Collection ll = ((Collection) tmp);
                            for (Object o : ll) {
                                getValor(o, parametro, new ArrayDeque<>(parametros), listaTmp);
                            }

                        }
                    } else {
                        return null;
                    }
                }

                if (tmp != null && tipo.getSuperclass() != null && tipo.getSuperclass().equals(EntidadeBasica.class)) {
                    try {
                        //testa se esta carregado, caso nao, LazyInitializationException eh lancada
                        ((EntidadeBasica) tmp).getCodigo();
                        Object valor = getValor(tmp, parametro, parametros, listaTmp);
                        return valor;
                    } catch (LazyInitializationException e) {
                        try {
                            Serializable id = ((HibernateProxy) tmp).getHibernateLazyInitializer().getIdentifier();
                            if (id != null) {
                                EntidadeBasica novoEntidade = (EntidadeBasica) tipo.newInstance();

                                Method mSetaValor = novoEntidade.getClass().getDeclaredMethod("setCodigo", novoEntidade.getClass().getDeclaredField("codigo").getType());
                                mSetaValor.invoke(novoEntidade, id);
                                Object valor = getValor(novoEntidade, parametro, parametros, listaTmp);
                                return valor;
                            }
                        } catch (ClassCastException cce) {
                            throw new RuntimeException(cce);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                } else if (tmp != null && tmp.getClass().isAnnotationPresent(Embeddable.class)) {
                    tmp = getValor(tmp, parametro, parametros, listaTmp);
                }
                return tmp;
            } catch (EntityNotFoundException e) {
                return null;
            }
        }
        listaTmp.add(entidade);
        return entidade;
    }

    /**
     * Converte o Objeto para a {@link EntidadeBasica} especificada
     *
     * @param objetoOrigem
     * @param entidadeClasse
     * @return
     */
    public static <E extends EntidadeBasica, F extends Convertable> E converter(F objetoOrigem, Class<E> entidadeClasse) {
        if (entidadeClasse != null && objetoOrigem != null) {
            try {
                E novo = (E) entidadeClasse.newInstance();

                Field[] declaredFields = objetoOrigem.getClass().getDeclaredFields();

                for (Field fieldOrigem : declaredFields) {
                    JpaParam jpaParam = getJpaAnnotation(fieldOrigem);
                    if (jpaParam != null) {
                        try {
                            List<Object> listaTmp = new ArrayList<>();
                            Object valorTmp = getValor(objetoOrigem, fieldOrigem.getName());
                            if (valorTmp != null && listaTmp.isEmpty()) {
                                listaTmp.add(valorTmp);
                            }
                            if (!listaTmp.isEmpty()) {
                                Object valor = processaCampo(fieldOrigem, jpaParam, listaTmp.size() > 1 ? listaTmp : listaTmp.get(0));
                                setaValor(valor, novo, jpaParam);
                            } else {
                                if (jpaParam.vazioParaNull()) {
                                    setaValor("", novo, jpaParam);
                                }
                            }

                        } catch (Exception e) {
                            //							e.printStackTrace();
                        }
                    }
                }

                return novo;

            } catch (InstantiationException | IllegalAccessException e) {
                //				e.printStackTrace();
            }
        }
        return null;
    }

    private static <E> void setaValor(Object novoValor, E entidade, JpaParam jpaParam)
                    throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Queue<String> parametros = new ArrayDeque<>(Arrays.asList(jpaParam.value().split("\\.")));
        setaValor(novoValor, entidade, jpaParam.permiteZero(), parametros);
    }

    private static <E> void setaValor(Object novoValor, E entidade, String jpaParam)
                    throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Queue<String> parametros = new ArrayDeque<>(Arrays.asList(jpaParam.split("\\.")));
        setaValor(novoValor, entidade, false, parametros);
    }

    private static <E> Queue<String> setaValor(Object novoValor, E entidade, boolean permiteZero, Queue<String> parametros)
                    throws NoSuchFieldException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        String param = parametros.poll();
        if (param != null) {

            Field fieldEntidade = entidade.getClass().getDeclaredField(param);

            Class<?> tipoValor = entidade.getClass().getDeclaredField(param).getType();
            if (fieldEntidade.getType().getSuperclass() != null && fieldEntidade.getType().getSuperclass().equals(EntidadeBasica.class)) {

                if (novoValor instanceof Number) {
                    if (((Number) novoValor).longValue() == 0 && !permiteZero) {
                        return null;
                    }
                } else if (novoValor instanceof String) {
                    if (((String) novoValor).isEmpty()) {
                        return null;
                    }
                }
                Object novo;
                try {
                    fieldEntidade.setAccessible(true);
                    novo = fieldEntidade.get(entidade);
                    if (novo == null) {
                        novo = fieldEntidade.getType().newInstance();
                    }
                } catch (Exception e) {
                    novo = fieldEntidade.getType().newInstance();
                }

                Method mSetaValor = entidade.getClass().getDeclaredMethod("set" + StringUtils.capitalize(param), tipoValor);
                mSetaValor.invoke(entidade, novo);

                return setaValor(novoValor, novo, permiteZero, parametros);
            } else if (novoValor instanceof EntidadeBasica) {
                novoValor = ((EntidadeBasica) novoValor).getCodigo();
            } else if (novoValor.getClass().isAnnotationPresent(Embeddable.class)) {
                novoValor = getValor(novoValor, param);
            } else if (fieldEntidade.isAnnotationPresent(EmbeddedId.class)) {
                Object novo = tipoValor.newInstance();

                Method mSetaValor = entidade.getClass().getDeclaredMethod("set" + StringUtils.capitalize(param), tipoValor);
                mSetaValor.invoke(entidade, novo);
                return setaValor(novoValor, novo, permiteZero, parametros);
            } else {
                try {
                    if (!(novoValor instanceof Collection) && tipoValor.getInterfaces()[0].equals(Collection.class)) {

                        Method setCollection;
                        try {
                            setCollection = entidade.getClass().getDeclaredMethod("set" + StringUtils.capitalize(param), tipoValor);
                        } catch (NoSuchMethodException e) {
                            setCollection = entidade.getClass().getDeclaredMethod("set" + param, tipoValor);
                        }

                        Class tipoGenerico = (Class) ((ParameterizedType) fieldEntidade.getGenericType()).getActualTypeArguments()[0];
                        Object objetoGenerico = tipoGenerico.newInstance();

						/*Field f = objetoGenerico.getClass().getDeclaredField("codigo");
						if (f.getType().isAnnotationPresent(Embeddable.class)) {
							Object o = f.getType().newInstance();

							Method mSvalor = objetoGenerico.getClass().getDeclaredMethod("setCodigo", tipoGenerico);
							setCollection.invoke(objetoGenerico, novoValor);
						}*/

                        Object novo;
                        if (tipoValor.equals(List.class)) {
                            List a = new ArrayList();
                            a.add(objetoGenerico);
                            novo = a;
                        } else {
                            Set a = new HashSet();
                            a.add(objetoGenerico);
                            novo = a;
                        }
                        setCollection.invoke(entidade, novo);

                        return setaValor(novoValor, objetoGenerico, permiteZero, parametros);

						/*Class atipo = (Class) ((ParameterizedType) fieldEntidade.getGenericType()).getActualTypeArguments()[0];
						Object n = atipo.newInstance();

						Field f = n.getClass().getDeclaredField("codigo");
						if(f.getType().isAnnotationPresent(EmbeddedId.class)){
							Object o = f.getType().newInstance();

							Method mSetaValor = o.getClass().getDeclaredMethod("setCodigo", atipo);
							mSetaValor.invoke(n, novoValor);
						}

						Method mSetaValor = n.getClass().getDeclaredMethod("setCodigo", atipo);
						mSetaValor.invoke(n, novoValor);

						Set novaLista = new HashSet();
						novaLista.add(n);

						if (tipoValor.equals(List.class)) {
							novoValor = new ArrayList<>(novaLista);
						} else {
							novoValor = novaLista;
						}*/

                    }
                } catch (Exception e) {
                    //					e.printStackTrace();
                }
            }

            try {
                Method mSetaValor = null;
                try {
                    mSetaValor = entidade.getClass().getDeclaredMethod("set" + StringUtils.capitalize(param), tipoValor);
                } catch (NoSuchMethodException e) {
                    mSetaValor = entidade.getClass().getDeclaredMethod("set" + param, tipoValor);
                }

                Object valorCorrigido = corrigeValor(novoValor, tipoValor);
                if (valorCorrigido != null) {
                    if (isID(fieldEntidade) && valorCorrigido instanceof Number && ((Number) valorCorrigido).intValue() == 0) {
                        valorCorrigido = null;
                    }
                    mSetaValor.invoke(entidade, valorCorrigido);
                }
            } catch (Exception e) {
                //                RisLogger.error(e);
            }

        } else if (entidade instanceof EntidadeBasica) {
            Method mSetaValor = entidade.getClass().getDeclaredMethod("setCodigo", entidade.getClass().getDeclaredField("codigo").getType());
            if (mSetaValor != null) {
                Object valor = corrigeValor(novoValor, entidade.getClass().getDeclaredField("codigo").getType());
                mSetaValor.invoke(entidade, valor);
            }
        }
        return null;
    }

    private static JpaParam getJpaAnnotation(AnnotatedElement field) {
        if (field.isAnnotationPresent(JpaParam.class)) {
            return field.getAnnotation(JpaParam.class);
        }
        return null;
    }

    public static Object corrigeValor(Object valor, Class tipo) {

        if (tipo.isAssignableFrom(String.class)) {
            if (valor instanceof Boolean) {
                return Boolean.parseBoolean(valor.toString()) ? "1" : "0";
            }
            return valor.toString();
        } else if (tipo.isAssignableFrom(BigDecimal.class)) {
            if (valor instanceof String) {
                return new BigDecimal((String) valor);
            } else if (valor instanceof Double) {
                return BigDecimal.valueOf((Double) valor);
            } else if (valor instanceof Integer) {
                return BigDecimal.valueOf((Integer) valor);
            } else if (valor instanceof BigInteger) {
                return new BigDecimal((BigInteger) valor);
            }
        } else if (tipo.isAssignableFrom(BigInteger.class)) {
            if (valor instanceof String) {
                return new BigInteger((String) valor);
            } else if (valor instanceof Integer) {
                return BigInteger.valueOf((Integer) valor);
            }
        } else if (tipo.isAssignableFrom(Long.class) || tipo.isAssignableFrom(long.class)) {
            if (valor instanceof String) {
                return Long.parseLong((String) valor);
            } else if (valor instanceof Integer) {
                return Long.valueOf(((Integer) valor).intValue());
            } else if (valor instanceof BigInteger) {
                return Long.valueOf(((BigInteger) valor).longValue());
            }
        } else if (tipo.isAssignableFrom(Double.class) || tipo.isAssignableFrom(double.class)) {
            if (valor instanceof String) {
                return Double.parseDouble((String) valor);
            } else if (valor instanceof Integer) {
                return Double.valueOf(((Integer) valor).intValue());
            } else if (valor instanceof BigInteger) {
                return ((BigInteger) valor).doubleValue();
            } else if (valor instanceof BigDecimal) {
                return ((BigDecimal) valor).doubleValue();
            }
        } else if (tipo.isAssignableFrom(Integer.class) || tipo.isAssignableFrom(int.class)) {
            if (valor instanceof String) {
                return Integer.parseInt((String) valor);
            } else if (valor instanceof Long) {
                return ((Long) valor).intValue();
            } else if (valor instanceof Collection) {

            }
        } else if (tipo.isAssignableFrom(Boolean.class) || tipo.isAssignableFrom(boolean.class)) {
            if (valor instanceof String) {
                return valor.toString().equals("1");
            } else if (valor instanceof Number) {
                return ((Number) valor).intValue() == 1;
            }
        } else if (tipo.isAssignableFrom(java.sql.Date.class)) {
            if (valor instanceof Date) {
                return new java.sql.Date(((Date) valor).getTime());
            } else if (valor instanceof String) {
                try {
                    return new SimpleDateFormat("yyyy-MM-dd").parse((String) valor);
                } catch (ParseException e) {}
            }
        }

        try {
            tipo.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            if ("float".equals(tipo.getName())) {
                if (valor instanceof BigDecimal) {
                    return ((BigDecimal) valor).floatValue();
                } else if (valor instanceof BigInteger) {
                    return ((BigInteger) valor).floatValue();
                }
            } else if ("double".equals(tipo.getName())) {
                if (valor instanceof BigDecimal) {
                    return ((BigDecimal) valor).doubleValue();
                } else if (valor instanceof BigInteger) {
                    return ((BigInteger) valor).doubleValue();
                }
            } else if ("int".equals(tipo.getName())) {
                if (valor instanceof BigDecimal) {
                    return ((BigDecimal) valor).intValue();
                } else if (valor instanceof BigInteger) {
                    return ((BigInteger) valor).intValue();
                }
            }
        }
        return valor;
    }

    /**
     * Cria uma lista de {@link Map<String,String>} com todos os gets das entidades
     *
     * @param entidades
     * @param <E>
     * @return
     */
    public static <E extends EntidadeBasica> List<Map<String, String>> toListMap(List<E> entidades) {

        if (entidades != null) {

            return entidades.stream().map(entidade -> {
                Map<String, String> tmpLista = new HashMap<>();
                Method[] methods = entidade.getClass().getDeclaredMethods();
                for (Method method : methods) {
                    if ((method.getName().startsWith("get") || method.getName().startsWith("is")) && Modifier.isPublic(method.getModifiers())) {
                        try {
                            Object obj = method.invoke(entidade);
                            if (obj != null) {
                                String key = method.getName().substring(method.getName().startsWith("get") ? 3 : 2, method.getName().length());

                                tmpLista.put(StringUtils.uncapitalize(key), obj.toString());
                            }
                        } catch (IllegalAccessException | InvocationTargetException | EntityNotFoundException | LazyInitializationException e) {
                        }
                    }
                }
                return tmpLista;
            }).collect(Collectors.toList());
        }
        return null;
    }

    public static <F extends Convertable, E extends EntidadeBasica> List<F> converter(List<E> entidades, Class<F> objetoClasse) {
        if (entidades != null && !entidades.isEmpty()) {
            return entidades.parallelStream().map(e -> e.converter(objetoClasse)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

}
