package br.com.eigmercados.websupport.interceptors;

import br.com.eigmercados.websupport.services.AbstractService;

import javax.interceptor.InterceptorBinding;
import javax.transaction.Transactional;
import java.lang.annotation.*;

/**
 * Anotacao que garante um ambiente transacional a classe.
 * Tambem garante o rollBack das transacoes dos metodos anotados com @{@link javax.transaction.Transactional}
 * na classe {@link AbstractService}
 */
@Transactional
@Inherited
@InterceptorBinding
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface RollBack {

}