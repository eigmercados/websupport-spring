package br.com.eigmercados.websupport.converters;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Deserializador que deve ser usado em todas as entidades que seram recebidas
 * atraves de uma requisição REST nas controllers da aplicação.
 * Ele trata o formato padrao de data  recebidos na requisição
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public LocalDate deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {

        LocalDate retorno = null;
        try {
            retorno = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(jsonparser.getText()));
        } catch (DateTimeParseException e) {
            try {
                LocalDateTime datetime = LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(jsonparser.getText()));
                retorno = datetime.toLocalDate();
            } catch (Exception ee) {

                try {
                    retorno = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(jsonparser.getText().substring(0, 10)));
                } catch (IOException e1) {
                    logger.debug(e1.getMessage());
                }

            }
        }

        return retorno;
    }
}