/*
* Projeto: framework-core
* Arquivo: SaudeI18NUtil.java
* 
* Copyright @ Ministério da Saúde.
*
* Este software é confidencial e de propriedade do Ministério da Saúde.
* Não é permitida sua distribuição ou divulgação do seu conteúdo sem 
* expressa autorização do mesmo.
*/
package br.com.eigmercados.websupport.geral.i18n;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Classe utilitária que obtem as mensagens contidas nos arquivos de propriedades de acordo com
 * o {@link Locale} da aplicação e formata os argumentos enviados por parâmetro, retornando a mensagem
 * a ser exibida para o usuário.<br>
 * 
 * Ex.: Supondo MSG001={0} incluído com sucesso, o retorno será: Usuário incluído com sucesso
 * 
 * <pre>
 * {@code
 * 
 * 	getProperty("MSG001", "Usuário");
 * }
 * </pre>
 * 
 * Caso a aplicação tenha o requisito de internacionalização este recurso pode ser alcançado da seguinte maneira:
 * <ul>
 * 	<li>
 * 		Inserindo um arquivo de propriedades com o mesmo nome seguido do sufixo da língua escolhida. Ou seja,
 * 		teríamos dois arquivos, um chamado de 'mensagens.properties', e o outro de 'mensagens_en.properties'
 * 		para mensagens traduzidas para o inglês
 * </li>
 *  <li>Definindo o {@link Locale} padrão com a língua escolhida
 * 		<pre>
 * 		{@code
 *			Locale.setDefault(Locale.US);
 *			Locale.setDefault(new Locale("pt", "BR"));
 * 		}
 * 		</pre>
 *  </li>
 * <ul>
  
 *
 * 
 */
@Component
public class I18NUtil {

	private final Log logger = LogFactory.getLog(getClass());


	@Autowired
	private ApplicationContext context;

	@Autowired
	private MessageSource messageSource;
	
	public I18NUtil(){}

	/**
	 * Método que retorna a mensagem correta que está contida dentro dos arquivos de 
	 * propriedades. Estas podem conter parâmetros dinâmicos Ex.: key=Mensagem {0} {1} 
	 * 
	 * @param chave Chave da mensagem
	 * @param params Parâmetros a serem inseridos na mensagem
	 * @return Mensagem tratada caso a mensagem seja encontrada nos arquivos de propriedades, caso
	 * contrário retorna a chave enviada
	 */
	public String getProperty(String chave, String... params) {
		
		try {
			return context.getMessage(chave, params, Locale.getDefault());
		} catch (NoSuchMessageException e) {
			logger.error("Key " + chave + " n\u00E3o encontrada nos arquivos de propriedade da aplica\u00E7\u00E3o");
			return chave;
		}
	}

	/**
	 * Método que retorna a mensagem correta que está contida dentro dos arquivos de 
	 * propriedades. Estas podem conter parâmetros dinâmicos Ex.: key=Mensagem {0} {1} 
	 * 
	 * @param i18n {@link I18N} com a chave da mensagem
	 * @param params Parâmetros a serem inseridos na mensagem
	 * @return Mensagem tratada caso a mensagem seja encontrada nos arquivos de propriedades, caso
	 * contrário retorna a chave enviada
	 */
	public String getProperty(I18N i18n, String... params) {
		try {
			return context.getMessage(i18n.chave(), params, Locale.getDefault());
		} catch (NoSuchMessageException e) {
			logger.error("Key " + i18n.chave() + " n\u00E3o encontrada nos arquivos de propriedade da aplica\u00E7\u00E3o");
			return  i18n.chave() ;
		}
	}
	
}
