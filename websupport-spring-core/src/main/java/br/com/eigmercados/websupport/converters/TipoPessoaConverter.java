package br.com.eigmercados.websupport.converters;

import br.com.eigmercados.websupport.model.TipoPessoa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TipoPessoaConverter implements AttributeConverter<TipoPessoa, String> {

    @Override
    public String convertToDatabaseColumn(TipoPessoa tipoPessoa) {
        return tipoPessoa.getCodigo();
    }

    @Override
    public TipoPessoa convertToEntityAttribute(String codigoTipoPessoa) {
        return TipoPessoa.getTipoPessoa(codigoTipoPessoa);
    }

}
