package br.com.eigmercados.websupport.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
public enum RegistroAtivo {

    ATIVO("Ativo"), INATIVO("Inativo");

    private String descricao;

    private RegistroAtivo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @JsonValue
    public boolean isAtivo() {
        return RegistroAtivo.ATIVO == this;
    }

    @JsonCreator
    public static RegistroAtivo fromBoolean(Boolean nome) {
        if (nome != null)
            return nome ? RegistroAtivo.ATIVO : RegistroAtivo.INATIVO;
        return null;
    }
}
