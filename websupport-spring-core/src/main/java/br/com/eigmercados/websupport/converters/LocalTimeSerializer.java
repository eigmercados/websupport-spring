package br.com.eigmercados.websupport.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Serializador que altera o formato padrão que as datas deveram ser entregues ao cliente.
 *
 */
public class LocalTimeSerializer extends JsonSerializer<LocalTime> {

    @Override
    public void serialize(LocalTime date, JsonGenerator gen, SerializerProvider provider) throws IOException {
        try {
            gen.writeString(date.format(DateTimeFormatter.ISO_TIME));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}