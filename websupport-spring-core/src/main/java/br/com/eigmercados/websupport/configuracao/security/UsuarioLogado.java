package br.com.eigmercados.websupport.configuracao.security;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

public interface UsuarioLogado<E> {

	E getPrincipal();

	void setPrincipal(E principal);

	boolean possuiAcesso(String role);

	void setDetails(Object details);

	void setIdUsuarioAutenticado(Serializable idUsuarioAutenticado);

	Serializable getIdUsuarioAutenticado();

	Collection<GrantedAuthority> getAuthorities();
}
