package br.com.eigmercados.websupport.services;

import br.com.eigmercados.websupport.estereotipo.NaoValidarQuandoExistir;
import br.com.eigmercados.websupport.estereotipo.ShallowValidation;
import br.com.eigmercados.websupport.geral.EigException;
import br.com.eigmercados.websupport.geral.i18n.I18NUtil;
import br.com.eigmercados.websupport.interceptors.RollBack;
import br.com.eigmercados.websupport.model.EntidadeBasica;
import br.com.eigmercados.websupport.repositorios.Repositorio;
import br.com.eigmercados.websupport.repositorios.RepositorioGenerico;
import br.com.eigmercados.websupport.utils.EigLogger;
import br.com.eigmercados.websupport.utils.EntidadeUtils;
import br.com.eigmercados.websupport.utils.FetchParametro;
import br.com.eigmercados.websupport.utils.QueryParametro;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * criado por bruno em 30/08/17.
 */
@RollBack
@Transactional(readOnly = true)
public abstract class AbstractService<E extends EntidadeBasica> extends RepositorioGenerico<E> implements Serializable {

	private static final long serialVersionUID = 8355454104338297239L;

	private final String MSG_OBRIGATORIO = "MSG_001";
	public static final String ERRO = "ERRO";
	public static final String DUPLICADO = "DUPLICADO";
	public static final String MSG_DUPLICADO = "MSG_006";

	@Autowired
	private CachedValidators cachedValidators;

	@Autowired
	private I18NUtil i18NUtil;

	protected Map<String, String> erros = new HashMap<>();

	protected abstract Repositorio<E> getRepositorio();

	/**
	 * Salva/atualiza a entidade
	 */
	@Transactional
	public Map<String, String> salvar(E entidade) throws EigException {
		return salvar(entidade, true);
	}

	/**
	 * Salvar/atualiza a entidade com a opcao de validar os obrigatorios ou nao
	 *
	 * @param entidade
	 * @param validar
	 * @return
	 * @throws RuntimeException
	 */
	@Transactional
	protected Map<String, String> salvar(E entidade, boolean validar) throws EigException {
		if (validar) {
			if (validarObrigatorios(entidade)) {
				inserirOuAtualizar(entidade);
			}
		} else {
			inserirOuAtualizar(entidade);
		}
		return erros;
	}

	/*    *//**
	 * Insere a entidade
	 *
	 * @param entidade
	 *//*
    @Transactional
    public void inserir(E entidade) {
        if (getRepositorio() != null) {
            getRepositorio().save(entidade);
        }
    }

    *//**
	 * Exclui a entidade
	 *
	 * @param entidade
	 *//*
    @Transactional
    public void excluir(E entidade) throws SncException {
        if (getRepositorio() != null && entidade != null) {
            getRepositorio().delete(entidade);
        }
    }*/

	/**
	 * Exclui as entidades pelos parametros passados
	 *
	 * @param parametros
	 */
	@Transactional
	public void excluir(List<QueryParametro> parametros) throws EigException {
		super.excluir(parametros);
	}

	/**
	 * Exclui a entidade
	 *
	 * @param entidades
	 */
	@Transactional
	public void excluir(Iterable<E> entidades) throws EigException {
		for (E entidade : entidades) {
			this.excluir(entidade);
		}
	}

	protected E recuperarPorAtributo(List<QueryParametro> atributos) {
		return super.recuperarPorAtributo(atributos);
	}

	@Override
	protected E recuperarPorAtributo(String nomeAtributo, Object valorAtributo) {
		return super.recuperarPorAtributo(nomeAtributo, valorAtributo);
	}

	protected E recuperarPorAtributo(String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		return super.recuperarPorAtributo(nomeAtributo, valorAtributo, fetchs);
	}

	protected E recuperarPorAtributo(List<QueryParametro> atributos, FetchParametro... fetchs) {
		return super.recuperarPorAtributo(atributos, fetchs);
	}

	/**
	 * Lista as entidades filtrando pelo atributo/valor
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @return
	 */
	@Override
	public List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo) {
		return super.listarPorAtributo(nomeAtributo, valorAtributo);
	}

	/**
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param joinType
	 * @return
	 */
	@Override
	protected List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, JoinType joinType) {
		return super.listarPorAtributo(nomeAtributo, valorAtributo, joinType);
	}

	/**
	 * Lista as entidades filtrando pelo atributo/valor efetuando os fetchs das relacionamentos informados
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param fetchs
	 * @return
	 */
	@Override
	public Page<E> listarPorAtributo(PageRequest paginacao, String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		return super.listarPorAtributo(paginacao, nomeAtributo, valorAtributo, fetchs);
	}

	/**
	 * Lista as entidades pelo atributo/valor ordenando pelo order
	 *
	 * @param nomeAtributo
	 * @param valorAtributo
	 * @param order
	 * @return
	 */
	@Override
	public List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, Order... order) {
		return super.listarPorAtributo(nomeAtributo, valorAtributo, order);
	}

	/**
	 * Lista as entidades pelos atributos/valores do mapa informado
	 *
	 * @param atributos
	 * @return
	 */
	public List<E> listarPorAtributo(List<QueryParametro> atributos) {
		return super.listarPorAtributo(atributos.toArray(new QueryParametro[] {}));
	}

	@Override
	public List<E> listarPorAtributo(List<QueryParametro> atributos, JoinType joinType) {
		return super.listarPorAtributo(atributos, joinType);
	}

	/**
	 * Lista as entidades pelos atributos/valores do mapa informado efetuando o fetch dos relacionamentos
	 *
	 * @param atributos
	 * @param fetchs
	 * @return
	 */
	@Override
	public List<E> listarPorAtributo(List<QueryParametro> atributos, FetchParametro... fetchs) {
		return super.listarPorAtributo(atributos, fetchs);
	}

	/**
	 * Lista as entidades e as ordena
	 *
	 * @param atributos
	 * @param order
	 * @return
	 */
	public List<E> listarPorAtributo(List<QueryParametro> atributos, Order... order) {
		return super.listarPorAtributo(atributos, null, order);
	}

	/**
	 * Lista as entidades e as ordena
	 *
	 * @param atributos
	 * @param order
	 * @return
	 */
	public List<E> listarPorAtributo(List<QueryParametro> atributos, int maxResult, Order... order) {
		return super.listarPorAtributo(atributos, null, maxResult, order);
	}

	/**
	 * Lista as entidades pelos atributos/valores do mapa informado utilizando o {@link JoinType} como tipo de junção da query e as ordena
	 *
	 * @param atributos
	 * @param orders
	 * @param fetchs
	 * @return
	 */
	@Override
	public List<E> listarPorAtributo(List<QueryParametro> atributos, List<Order> orders, FetchParametro... fetchs) {
		return super.listarPorAtributo(atributos, orders, fetchs);
	}

	@Override
	public List<E> listarPorAtributo(String nomeAtributo, Object valorAtributo, FetchParametro... fetchs) {
		return super.listarPorAtributo(nomeAtributo, valorAtributo, fetchs);
	}

	/**
	 * Faz a busca na entidade pelo campo/termo usando o like
	 *
	 * @param campo
	 * @param termo
	 * @return
	 */
	@Override
	public List<E> autocomplete(String campo, String termo) {
		return super.autocomplete(campo, termo);
	}

	/**
	 * Atualiza a entidade
	 *
	 * @param entidade
	 * @return
	 */
/*    @Transactional
    public E atualizar(E entidade) {
        if (getRepositorio() != null) {
            return getRepositorio().save(entidade);
        }
        return entidade;
    }*/

	/**
	 * Valida todos os obrigatorios da entidade baseando se no atributo nullable das anotacoes @{@link Column} e @{@link JoinColumn}
	 *
	 * @param entidade
	 * @return
	 */
	public boolean validarObrigatorios(E entidade) {
		EntidadeUtils.removeLazys(entidade);
		return /*validarObrigatoriosMetodos(entidade, null) && */validarObrigatoriosCampos(entidade, null);
	}

	private boolean validarObrigatoriosMetodos(E entidade, Method pai) {

		if (entidade != null) {

			if (cachedValidators.metodoValido(entidade)) {
				return true;
			}

			boolean check = true;
			for (Method method : entidade.getClass().getDeclaredMethods()) {
				if (isRequired(method)) {
					method.setAccessible(true);
					try {
						Object obj = method.invoke(entidade);
						if (obj instanceof EntidadeBasica) {
							if (!validarObrigatoriosMetodos((E) obj, method)) {
								check = false;
								addErro(method.getName(), pai != null ? pai.getName() : null);
							}
						} else if (obj == null) {
							check = false;
							addErro(method.getName(), pai != null ? pai.getName() : null);
						}

					} catch (IllegalAccessException | InvocationTargetException e) {

					} finally {
						method.setAccessible(false);
					}
				}
			}
			cachedValidators.putMetodos(entidade, check);
			return check;

		}
		return false;
	}

	private boolean validarObrigatoriosCampos(E entidade, Field pai) {

		if (entidade != null) {

			if (cachedValidators.campoValido(entidade)) {
				return true;
			}

			executaPrePersist(entidade);

			boolean check = true;
			for (Field field : entidade.getClass().getDeclaredFields()) {
				if (isRequired(field)) {
					try {
						field.setAccessible(true);
						Object obj = field.get(entidade);
						if (EntidadeUtils.isLazyLoaded(obj)) {
							continue;
						}
						if (obj instanceof EntidadeBasica && !isShallow(field)) {
							if (!validarObrigatoriosCampos((E) obj, field)) {
								check = false;
								addErro(field.getName(), pai != null ? pai.getName() : null);
							}
						} else if (obj == null) {
							String ignorarQuandoExistir = isIgnorarQuandoExistir(field);
							if (ignorarQuandoExistir != null) {
								Method declaredMethod = entidade.getClass().getDeclaredMethod("get" + StringUtils.capitalize(ignorarQuandoExistir));
								if (declaredMethod.invoke(entidade) != null) {
									continue;
								}
							}
							check = false;
							addErro(field.getName(), pai != null ? pai.getName() : null);
						}

					} catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
					} finally {
						field.setAccessible(false);
					}
				}
			}
			cachedValidators.putCampos(entidade, check);
			return check;
		}
		return false;
	}

	private void executaPrePersist(E entidade) {
		if (entidade != null) {
			for (Method method : entidade.getClass().getDeclaredMethods()) {
				if (method.isAnnotationPresent(PrePersist.class)) {
					try {
						method.setAccessible(true);
						method.invoke(entidade);

					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}

		}
	}

	private void addErro(String metodo, String pai) {
		String key = pai != null ? pai.replace("get", "") + "#" + metodo.replace("get", "") : metodo.replace("get", "");
		erros.put(key, getMensagem(MSG_OBRIGATORIO, key));
	}

	protected String getMensagem(String key, String... params) {
		return i18NUtil.getProperty(key, params);
	}

	private boolean isRequired(AnnotatedElement method) {
		if (method.isAnnotationPresent(Id.class)) {
			return false;
		}
		Column coluna = method.getAnnotation(Column.class);
		if (coluna != null) {
			return !coluna.nullable();
		}

		JoinColumn join = method.getAnnotation(JoinColumn.class);
		if (join != null) {
			if (join.insertable() && join.updatable()) {
				return !join.nullable();
			}
		}

		return false;
	}

	private boolean isShallow(AnnotatedElement method) {
		return method.isAnnotationPresent(ShallowValidation.class);
	}

	private String isIgnorarQuandoExistir(AnnotatedElement method) {
		if (method.isAnnotationPresent(NaoValidarQuandoExistir.class)) {
			NaoValidarQuandoExistir annotation = method.getAnnotation(NaoValidarQuandoExistir.class);
			return annotation.atributo();
		}
		return null;
	}

	public Map<String, String> getErros() {
		return Collections.unmodifiableMap(erros);
	}

	public void clearErros() {
		erros.clear();
	}

	/**
	 * Verifica se existe um registro igual a {@link EntidadeBasica} informada, filtrando pelos parametros informados
	 *
	 * @param entidade
	 * @param params
	 * @return
	 */
	protected boolean validarDuplicado(E entidade, List<QueryParametro> params) {
		return validarDuplicado(entidade, params, MSG_DUPLICADO);
	}

	/**
	 * Verifica se existe um registro igual a {@link EntidadeBasica} informada, filtrando pelos parametros informados
	 *
	 * @param entidade
	 * @param params
	 * @param mensagemErro
	 * @return
	 */
	protected boolean validarDuplicado(E entidade, List<QueryParametro> params, String mensagemErro, FetchParametro... fetchs) {

		List<E> entidades = listarPorAtributo(params, fetchs);

		if (!entidades.isEmpty()) {
			if (!entidades.contains(entidade)) {
				erros.put(DUPLICADO, mensagemErro);
				return false;
			}

		}
		return true;
	}

	/**
	 * Verifica se existe um registro igual a {@link EntidadeBasica} informada, filtrando pelos parametros informados e validando pelo atributo informado.
	 *
	 * @param entidade
	 * @param params
	 * @param mensagemErro
	 * @return
	 */
	protected boolean validarDuplicado(E entidade, List<QueryParametro> params, String atributo, String mensagemErro) {

		List<E> entidades = listarPorAtributo(params);

		if (!entidades.isEmpty()) {
			Method metodoEntidade = null;
			Method metodoRetornoLista = null;
			String methodName = (new StringBuilder("get")).append(atributo).toString();
			for (Method m : entidade.getClass().getMethods()) {
				if (m.getName().equalsIgnoreCase(methodName)) {
					metodoEntidade = m;
					break;
				}
			}

			for (Object e : entidades) {
				for (Method m : e.getClass().getMethods()) {
					if (m.getName().equalsIgnoreCase(methodName)) {
						metodoRetornoLista = m;
						if (metodoEntidade == null || metodoRetornoLista == null) {
							throw new RuntimeException("Nao foi poss\u00EDvel recuperar o atribudo para valida\u00E7\u00E3o.");
						} else {
							if (!metodoEntidade.getReturnType().equals(metodoRetornoLista.getReturnType())) {
								throw new RuntimeException("Os objetos comparados possuem retornos diferentes;");
							} else {
								try {
									if (metodoEntidade.invoke(entidade).equals(metodoRetornoLista.invoke(e))) {
										erros.put(DUPLICADO, mensagemErro);
										return false;
									}
								} catch (IllegalAccessException e1) {
									EigLogger.error(e1);
								} catch (InvocationTargetException e1) {
									EigLogger.error(e1);
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
}