package br.com.eigmercados.websupport.estereotipo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 * Anotacao que vincula o atributo ao atributo da entidade do banco de dados
 * Essa anotacao eh necessario para o motor de conversao dos Objects para as Entidades
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JpaParam {

    /**
     * Nome do atributo da entidade
     *
     * @return
     */
    String value();

    /**
     * Em caso do value ser uma EntidadeBasica, indica se o valor da @Id aceita ou nao o valor 0 (zero)
     *
     * @return
     */
    boolean permiteZero() default false;

    /**
     * Converte o atributo, que foi definido no value, para a classe informada
     *
     * @return
     */
    Class<? extends Convertable> converterPara() default Convertable.class;

    /**
     * Caso o objeto esteja nulo, enviar uma string vazia no lugar
     * @return
     */
    boolean vazioParaNull() default false;

    /**
     * Caso o objeto for uma collection, o primeiro item sera utilizado.
     * @return
     */
    boolean primeiroDaLista() default true;
}

