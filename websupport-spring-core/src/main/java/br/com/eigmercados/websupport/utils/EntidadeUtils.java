package br.com.eigmercados.websupport.utils;

import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.format.CPFFormatter;
import br.com.eigmercados.websupport.model.EntidadeBasica;
import org.hibernate.LazyInitializationException;
import org.hibernate.proxy.HibernateProxy;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class EntidadeUtils {

    private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
    private static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

    public static <E extends EntidadeBasica> void removeLazys(E entidade) {
        removeLazys(entidade, new HashSet<>());
    }

    private static <E extends EntidadeBasica> void removeLazys(E entidade, Set<String> cached) {
        if (!cached.contains(entidade.getUniqueName())) {
            cached.add(entidade.getUniqueName());
            for (Method method : entidade.getClass().getDeclaredMethods()) {
                if (Modifier.isPublic(method.getModifiers()) && method.getName().startsWith("get")) {
                    if (isEntidadeBasica(method)) {
                        try {
                            Object obj = method.invoke(entidade);
                            if (obj != null && !cached.contains(((E) obj).getUniqueName())) {
                                removeLazys((E) obj, cached);
                            }
                        } catch (LazyInitializationException e) {
                            try {
                                String setMetodo = "s" + method.getName().substring(1, method.getName().length());
                                Method set = entidade.getClass().getDeclaredMethod(setMetodo, method.getReturnType());
                                set.invoke(entidade, new Object[] { null });
                            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ee) {
                            }

                        } catch (IllegalAccessException | InvocationTargetException e) {
                        }
                    }
                }
            }
        }
    }

    private static boolean isEntidadeBasica(Method method) {
        try {
            Class clazz = getGenericReturnType(method);
            return method.getReturnType().newInstance() instanceof EntidadeBasica || (clazz != null && clazz.isAssignableFrom(EntidadeBasica.class));
        } catch (Exception e) {
            return false;
        }
    }

    private static Class getGenericReturnType(Method method) {
        if (method.getGenericReturnType() != null && method.getGenericReturnType().getClass().isAssignableFrom(ParameterizedType.class)) {
            ParameterizedType parameterizedType = (ParameterizedType) method.getGenericReturnType();
            if (parameterizedType.getActualTypeArguments().length > 0) {
                return (Class) parameterizedType.getActualTypeArguments()[0];
            }
        }
        return null;
    }

    public enum TipoLista {
        INCLUSAO, EXCLUCAO;
    }

    public static <E extends EntidadeBasica> Map<TipoLista, Collection<E>> resolverInclusaoExclucao(Collection<E> listaOriginal, Collection<E> listaNova) {
        if (listaOriginal != null && listaNova != null) {

            List<E> listaOrigem = new ArrayList<>(listaOriginal);

            List<E> listaInclusao = new ArrayList<>(listaNova);
            List<E> listaDelecao = new ArrayList<>(listaNova);
            listaInclusao.removeAll(listaOrigem);
            listaOrigem.removeAll(listaDelecao);

            return new HashMap<TipoLista, Collection<E>>() {{
                put(TipoLista.INCLUSAO, listaInclusao);
                put(TipoLista.EXCLUCAO, listaOrigem);
            }};

        }
        return null;
    }

    public static boolean isLazyLoaded(Object object) {
        if (object instanceof HibernateProxy) {
            if (((HibernateProxy) object).getHibernateLazyInitializer().isUninitialized()) {
                return true;
            }
        }
        return false;
    }

    public static String cnpjFormatado(BigDecimal cnpj) {
        String tmp = String.format("%14s", cnpj.toString()).replaceAll(" ", "0");
        CNPJFormatter cnpjFormatter = new CNPJFormatter();
        return cnpjFormatter.format(tmp);
    }

    public static String cpfFormatado(BigDecimal cnpj) {
        String tmp = String.format("%11s", cnpj.toString()).replaceAll(" ", "0");
        CPFFormatter cpfFormatter = new CPFFormatter();
        return cpfFormatter.format(tmp);
    }

    public static String cpfOuCnpjFormatado(BigDecimal valor) {
        if (validarCPF(valor)) {
            return cpfFormatado(valor);
        }
        return cnpjFormatado(valor);
    }

    public static Date getDate(LocalDate date) {
        if (date == null) return null;
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDate(LocalDateTime date) {
        if (date == null) return null;
        return Date.from(date.toInstant(ZoneOffset.UTC));
    }

    public static LocalDateTime toLocalDateTime(LocalDate date) {
        if (date == null) return null;
        return date.atStartOfDay();
    }

    public static boolean validaCpfOuCnpj(String cpfCnpj) {
        return (validarCPF(cpfCnpj) || validarCnpj(cpfCnpj));
    }

    private static int calcularDigito(String str, int[] peso) {
        int soma = 0;
        for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
            digito = Integer.parseInt(str.substring(indice, indice + 1));
            soma += digito * peso[peso.length - str.length() + indice];
        }
        soma = 11 - soma % 11;
        return soma > 9 ? 0 : soma;
    }

    private static String padLeft(String text, char character) {
        return String.format("%11s", text).replace(' ', character);
    }

    public static boolean validarCPF(BigDecimal cpf) {
        String tmp = String.format("%11s", cpf.toString()).replaceAll(" ", "0");
        return validarCPF(tmp);
    }

    public static boolean validarCPF(String cpf) {
        cpf = cpf.trim().replace(".", "").replace("-", "");
        if ((cpf == null) || (cpf.length() != 11)) return false;

        for (int j = 0; j < 10; j++)
            if (padLeft(Integer.toString(j), Character.forDigit(j, 10)).equals(cpf))
                return false;

        Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
        Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
        return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
    }

    public static boolean validarCnpj(BigDecimal cnpj) {
        String tmp = String.format("%14s", cnpj.toString()).replaceAll(" ", "0");
        return validarCnpj(tmp);
    }

    public static boolean validarCnpj(String cnpj) {
        cnpj = cnpj.trim().replace(".", "").replace("-", "");
        if ((cnpj == null) || (cnpj.length() != 14)) return false;

        Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
        Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
        return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
    }

    public static String getDataPorExtenso(LocalDate localdate) {
        if (localdate == null) return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy");
        return localdate.format(formatter);
    }

    public static <E extends EntidadeBasica> boolean isEntidadeVazia(E entidade, boolean filtraEntidade) {
        Field[] declaredFields = entidade.getClass().getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (Modifier.isStatic(declaredField.getModifiers()) || Modifier.isTransient(declaredField.getModifiers())) {
                continue;
            }
            declaredField.setAccessible(true);
            try {
                Object obj = declaredField.get(entidade);
                if (obj != null) {
                    if (filtraEntidade && !(obj instanceof EntidadeBasica)) {
                        return false;
                    }else if(!filtraEntidade){
                        return false;
                    }
                }
            } catch (IllegalAccessException e) {
            }
        }

        return true;
    }

}
