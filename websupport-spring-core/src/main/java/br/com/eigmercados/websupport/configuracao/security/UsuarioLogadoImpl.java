package br.com.eigmercados.websupport.configuracao.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;

/**
 * Objeto que representa o usuario logado na aplicacao
 */
@Getter
public class UsuarioLogadoImpl<E> extends User implements UsuarioLogado<E> {

    private static final long serialVersionUID = -6486193190946385379L;

    private Object details;
    private E principal;
    private Serializable idUsuarioAutenticado;

    public UsuarioLogadoImpl(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    @Override
    public void setIdUsuarioAutenticado(Serializable idUsuarioAutenticado) {
        this.idUsuarioAutenticado = idUsuarioAutenticado;
    }

    @Override
    public void setDetails(Object details) {
        this.details = details;
    }

    @Override
    public boolean possuiAcesso(String role) {
        return getAuthorities().stream().anyMatch(o -> o.getAuthority().equals(role));
    }

    @Override
    public void setPrincipal(E principal) {
        this.principal = principal;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return super.getAuthorities();
    }
}
