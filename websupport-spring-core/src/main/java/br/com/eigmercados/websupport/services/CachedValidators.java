package br.com.eigmercados.websupport.services;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.HashMap;
import java.util.Map;

@RequestScope
@Component
public class CachedValidators {

    private Map<Object,Boolean> cacheObjsMetodosValidados = new HashMap<>();
    private Map<Object,Boolean> cacheObjsCamposValidados = new HashMap<>();

    public void putMetodos(Object object,boolean valido){
        cacheObjsMetodosValidados.put(object,valido);
    }

    public void putCampos(Object object, boolean valido){
        cacheObjsCamposValidados.put(object,valido);
    }

    public boolean metodoValido(Object object){
        Boolean obj = cacheObjsMetodosValidados.get(object);
        return obj!=null && obj;
    }

    public boolean campoValido(Object object){
        Boolean obj = cacheObjsCamposValidados.get(object);
        return obj!=null && obj;
    }

}
