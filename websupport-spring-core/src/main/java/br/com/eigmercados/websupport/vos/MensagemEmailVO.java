package br.com.eigmercados.websupport.vos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MensagemEmailVO {
    private String assunto;
    private List<String> destinatarios;
    private String remetente;

    private String mensagem;
}
