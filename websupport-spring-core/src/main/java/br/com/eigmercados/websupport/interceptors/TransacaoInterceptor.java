package br.com.eigmercados.websupport.interceptors;

import br.com.eigmercados.websupport.services.AbstractService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.Session;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Map;

@Configuration
@Aspect
public class TransacaoInterceptor implements Serializable {

	private static final long serialVersionUID = -1939469955361094666L;

	@PersistenceContext
	private EntityManager entityManager;

	@Around("execution(* br.com.eigmercados.websupport.services..*(..))")
	public Object auditMethod(ProceedingJoinPoint jp) throws Throwable {

		Session session = entityManager.unwrap(Session.class);
		Object proceed = jp.proceed();

		MethodSignature signature = (MethodSignature) jp.getSignature();
		Method method = signature.getMethod();

		if (method != null && method.isAnnotationPresent(Transactional.class)) {
			Map erros = ((AbstractService) jp.getTarget()).getErros();
			if (erros != null && !erros.isEmpty()) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				/*if (session != null && session.getTransaction() != null) {
					session.getTransaction().rollback();
				}*/
			}
		}

		return proceed;

	}
}
