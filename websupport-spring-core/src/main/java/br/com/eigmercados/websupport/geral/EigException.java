package br.com.eigmercados.websupport.geral;

import br.com.eigmercados.websupport.geral.i18n.I18NUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Representa uma exceção genérica lançada pelo sistema.
 */
@Component
public class EigException extends RuntimeException {

	private static final long serialVersionUID = -3967682896736929553L;
	private String[] params;

	public EigException() {
	}

	@Autowired
	private I18NUtil i18NUtil;

	public EigException(Exception e) {
		super(e);
		resolveI18n();
	}

	public EigException(String mensagem) {
		super(mensagem);
		resolveI18n();
	}

	public EigException(String mensagem, Throwable erro) {
		super(mensagem, erro);
		resolveI18n();
	}

	public EigException(String mensagem, Throwable erro, String... params) {
		super(mensagem, erro);
		this.params = params;
	}

	private void resolveI18n() {
		if (i18NUtil == null) {
			i18NUtil = EigApplicationProvider.getBean(I18NUtil.class);
		}
	}

	private String resolverMsg(String key) {
		return i18NUtil.getProperty(key, params);
	}

	public String getMessage() {
		String mensagem = resolverMsg(getMensagemCausa());
		return mensagem;
	}

	private String getMensagemCausa() {
		try {
			return getMensagemCausa(this.getCause());
		} catch (Exception e) {
			return super.getMessage();
		}
	}

	private String getMensagemCausa(Throwable e) {
		if (e != null && e.getCause() != null) {
			return getMensagemCausa(e.getCause());
		}
		return e.getMessage();
	}
}
