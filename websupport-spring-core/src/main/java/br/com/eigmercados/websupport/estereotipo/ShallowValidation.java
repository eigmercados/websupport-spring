package br.com.eigmercados.websupport.estereotipo;

import br.com.eigmercados.websupport.model.EntidadeBasica;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 * Anotacao que indicara que o objeto tera uma validacao rasa, ou seja,
 * o metodo {@link br.com.eigmercados.websupport.services.AbstractService#validarObrigatorios(EntidadeBasica)} nao entrara
 * em todos os filhos deste objeto.
 *
 * Melhor usar esta anotacao para evitar {@link org.hibernate.LazyInitializationException} em objetos que nao sao
 * necessarios o carregamento.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ShallowValidation {

}
