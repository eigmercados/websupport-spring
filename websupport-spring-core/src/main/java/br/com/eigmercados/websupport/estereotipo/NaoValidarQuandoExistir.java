package br.com.eigmercados.websupport.estereotipo;


import br.com.eigmercados.websupport.model.EntidadeBasica;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 * Anotacao que indicara que este atributo nao sera validado caso o {@link EntidadeBasica#getCodigo()} estiver preenchido,
 * o metodo {@link br.com.eigmercados.websupport.services.AbstractService#validarObrigatorios(EntidadeBasica)}
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NaoValidarQuandoExistir {

    String atributo() default "codigo";
}
