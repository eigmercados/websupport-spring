FROM openjdk:8-jdk-alpine
VOLUME ["/tmp","/var/log"]

EXPOSE 12311

ARG PROFILE=dev
ENV SPRING_PROFILES_ACTIVE=${PROFILE}
ENV JAVA_OPTIONS=""
COPY web/target/web.jar app.jar

RUN apk add --no-cache tzdata curl
ENV TZ=America/Sao_Paulo

ENV SCRIPT="java ${JAVA_OPTIONS} \
             -XX:+UnlockExperimentalVMOptions\
             -XX:+UseCGroupMemoryLimitForHeap\
             -noverify -Dspring.profiles.active=${PROFILE}\
             -Dspring.output.ansi.enabled=always\
             -Dspring.liveBeansView.mbeanDomain\
             -Dspring.application.admin.enabled=true\
             -Dfile.encoding=UTF-8\
             -Djava.security.egd=file:/dev/./urandom -jar /app.jar"

CMD ${SCRIPT}